******************************************************************************
ndaal - Vulnerabilities - log4j - Check
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Overview:

What are the Limitations of Binary Based Identification?
------------------------------------------------------------------------------

The flexibility of the JAR format allows for JAR files to take many shapes 
and forms.
They can have several levels of hierarchy (a JAR, within a JAR, within a JAR, 
etc.) and have different extensions. Each type usually reflects a different 
packaging and deployment schema and can have specific requirements.

Common examples include:

-  **TAR**; 
   Tape Archive (uncompressed)
-  **WAR**; 
   Web Application Archive. Should contain a web.
   xml file in the WEB-INF folder
-  **EAR**;
   Enterprise Application Archive
-  **SAR**;
   Service Application Archive
-  **PAR**;
   Portlet Archive
-  **RAR**;
   Resource Adapter. A system-level driver that connects a Java application to
   an enterprise information system. Contains source code and an ra.xml that 
   serves as a deployment descriptor
-  **KAR**; 
   Apache Karaf Archive [20]_ . A special type of artifact that package a 
   features XML and all resources described in the features of this XML 
   (bundle JARs)
-  **Uber JAR/Fat JAR**;
   a JAR that contains a Java program as well as all of its dependencies. 
   The JAR serves as an all-in-one distribution of the software, without 
   needing any other Java code. It can be shaded, unshaded, or nested

   -  **Unshaded JAR**;
      Unpack all JAR files, then repack them into a single JAR
   -  **Shaded JAR**; 
      same as unshaded, but rename (i.e., "shade") all packages of all 
      dependencies
   -  **Nested JAR**;
      a JAR file that contains all other required JAR files (can have several 
      levels of hierarchy)

ndaal - Vulnerabilities - log4j - Check
------------------------------------------------------------------------------

We were checking the following scripts, tools to check the vulnerability 
scanning capabilities:

- https://github.com/fullhunt/log4j-scan [1]_ 
- https://github.com/Neo23x0/log4shell-detector [2]_ 
- https://github.com/NorthwaveSecurity/log4jcheck [3]_ 
- https://github.com/hillu/local-log4j-vuln-scanner [4]_ 
- https://github.com/adilsoybali/Log4j-RCE-Scanner [5]_ 
- https://github.com/takito1812/log4j-detect [6]_ 
- https://github.com/Diverto/nse-log4shell [7]_ 
- https://github.com/mergebase/log4j-detector [8]_ 
- https://github.com/palantir/log4j-sniffer [9]_
- https://github.com/CERTCC/CVE-2021-44228_scanner [10]_
- https://github.com/anchore/grype [11]_
- https://github.com/jfrog/log4j-tools [12]_ 
- https://github.com/CrowdStrike/CAST [13]_ 
- https://github.com/JagarYousef/log4j-dork-scanner [14]_ 
- https://github.com/Qualys/log4jscanwin [15]_ 

https://github.com/fullhunt/log4j-scan
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Support for lists of URLs.
- Fuzzing for more than 60 HTTP request headers (not only 3-4 headers as previously seen tools).
- Fuzzing for HTTP POST Data parameters.
- Fuzzing for JSON data parameters.
- Supports DNS callback for vulnerability discovery and validation.
- WAF Bypass payloads.

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We have been researching the Log4J RCE (CVE-2021-44228) since it was released, 
and we worked in preventing this vulnerability with our customers. We are 
open-sourcing an open detection and scanning tool for discovering and fuzzing 
for Log4J RCE CVE-2021-44228 vulnerability. This shall be used by security 
teams to scan their infrastructure for Log4J RCE, and also test for WAF 
bypasses that can result in achiving code execution on the organization's 
environment.

It supports DNS OOB callbacks out of the box, there is no need to setup a DNS 
callback server.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Most promising candidate for checking.

https://github.com/Neo23x0/log4shell-detector
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It does: It checks local log files for indicators of exploitation attempts, 
even heavily obfuscated ones that string or regular expression based patterns 
wouldn't detect.

- It doesn't find vulnerable applications
- It doesn't and can't verify if the exploitation attempts were successful

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Detector for Log4Shell exploitation attempts

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

https://github.com/NorthwaveSecurity/log4jcheck
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following HTTP headers are covered:

- X-Api-Version
- User-Agent
- Referer
- X-Druid-Comment
- Origin
- Location
- X-Forwarded-For
- Cookie
- X-Requested-With
- X-Forwarded-Host
- Accept

For each injection, the following JNDI prefixes are checked:

- jndi:rmi
- jndi:ldap
- jndi:dns
- jndi:${lower:l}${lower:d}ap

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Northwave created a testing script that checks for vulnerable systems using 
injection of the payload in common HTTP headers and as a part of a HTTP GET 
request. Vulnerable systems are detected by listening for incoming DNS 
requests that contain a UUID specically created for the target. By listening 
for incoming DNS instead of deploying (for example) an LDAP server, we 
increase the likelyhood that vulnerable systems can be detected that have 
outbound traffic filtering in place. In practice, outbound DNS is often 
allowed.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

https://github.com/hillu/local-log4j-vuln-scanner
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is a simple tool that can be used to find vulnerable instances of 
log4j 1.x and 2.x (CVE-2019-17571, CVE-2021-44228) in installations of Java 
software such as web applications. JAR and WAR archives are inspected and 
class files that are known to be vulnerable are flagged. The scan happens 
recursively: WAR files containing WAR files containing JAR files containing 
vulnerable class files ought to be flagged properly.

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This tool currently checks for known build artifacts that have been obtained 
through Maven. From-source rebuilds as they are done for Linux distributions 
may not be recognized.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

a candidate 

https://github.com/adilsoybali/Log4j-RCE-Scanner
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- It can scan according to the url list you provide.
- It can scan all of them by finding the subdomains of the domain name you give.
- It adds the source domain as a prefix to determine from which source the incoming dns queries are coming from.

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Using this tool, you can scan for remote command execution vulnerability 
CVE-2021-44228 on Apache Log4j at multiple addresses.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

https://github.com/takito1812/log4j-detect
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To do so, it sends a GET request using threads (higher performance) to each of 
the URLs in the specified list. The GET request contains a payload that on 
success returns a DNS request to Burp Collaborator / interactsh. This payload 
is sent in a test parameter and in the "User-Agent" / "Referer" / 
"X-Forwarded-For" / "Authentication" headers. Finally, if a host is 
vulnerable, an identification number will appear in the subdomain prefix of 
the Burp Collaborator / interactsh payload and in the output of the script, 
allowing to know which host has responded via DNS.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It should be noted that this script only handles DNS detection of the 
vulnerability and does not test remote command execution.

https://github.com/Diverto/nse-log4shell
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Nmap NSE scripts to check against log4shell or LogJam vulnerabilities 
(CVE-2021-44228). NSE scripts check most popular exposed services on the 
Internet. It is basic script where you can customize payload.

Note that NSE scripts will only issue the requests to the services. Nmap will 
not report vulnerable hosts, but you have to check DNS logs to determine 
vulnerability. Also note that DNS resolution with prefixes combination in a 
expression for log4j-core <= 2.7 seems not supported. So, testing with 
something like ${java:os} could lead to false negatives. Therefore, better to 
have few false positives than negatives.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

https://github.com/mergebase/log4j-detector
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Detects Log4J versions on your file-system within any application that are 
vulnerable to CVE-2021-44228 and CVE-2021-45046. It is able to even find 
instances that are hidden several layers deep. Works on Linux, Windows, and 
Mac, and everywhere else Java runs, too!

Currently reports log4j-core versions 2.12.2 and 2.16.0 as _SAFE_, 2.15.0 as 
_OKAY_ and all other versions as _VULNERABLE_ 
(although it does report pre-2.0-beta9 as "_POTENTIALLY_SAFE_").

Can correctly detect log4j inside executable spring-boot jars/wars, 
dependencies blended into uber jars, shaded jars, and even exploded jar 
files just sitting uncompressed on the file-system (aka *.class).

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Be evil do not seek for flaws in Java with Java. If the system is hacked 
what do you expect ...

At the end of the day it can be the problem that when the class and the war 
files are renamed then the tool will maybe not discover the flaws. 

We think on the long run at least the following measures have to be done 
analyze of the process list, log analyze und analyze of communication 
relation ships.

Some scanners did better than others, but none was able to
detect all formats.

Found 

- log4j versions
- Uber JAR
- Shaded JAR
- Shaded JAR
- Compressed JAR (ZIP)

not found

- Compressed archived JAR (GZIP)
- PAR file
- PAR file within a ZIP
- Compressed archived Uber JAR

https://github.com/palantir/log4j-sniffer
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

log4j-sniffer crawls for all instances of log4j that are earlier than version 
2.16 on disk within a specified directory. It can be used to determine whether 
there are any vulnerable instances of log4j within a directory tree.

Scanning for CVE-2021-45046 and CVE-2021-45105 is currently supported.

What this does
log4j-sniffer will scan a filesystem looking for all files of the following 
types based upon suffix:

- Zips: .zip
- Java archives: .jar, .war, .ear
- Tar: .tar, .tar.gz, .tgz, .tar.bz2, .tbz2
- Archives containing other archives will be recursively inspected up to a 
  configurable maximum depth. See the log4j-sniffer crawl --help output for 
  options on nested archive inspection.

It will look for the following:

- Jar files matching log4j-core-<version>.jar, including those nested within 
  another archive
- Class files named org.apache.logging.log4j.core.net.JndiManager within 
  Jar files or other archives and check against md5 hashes of known versions
- Class files named JndiManager in other package hierarchies and check against 
  md5 hashes of known versions
- Matching of the bytecode of classes named JndiManager against known classes 
  (see below for more details)
- Matching of bytecode within obfuscated or shaded jars for partial matches 
  against known classes (see below)

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you trust Palantir software then it is a partly feature complete tool.

Some scanners did better than others, but none was able to
detect all formats.

Found 

- log4j versions
- Uber JAR
- Shaded JAR
- Shaded JAR
- Compressed JAR (ZIP)
- Compressed archived JAR (GZIP)
- PAR file

not found

- PAR file within a ZIP
- Compressed archived Uber JAR

https://github.com/CERTCC/CVE-2021-44228_scanner
##############################################################################

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Applications that are vulnerable to the log4j CVE-2021-44228 issue may be 
detectable by scanning jar, war, and ear files to search for the presence of 
``JndiLookup.class``.

Depending on the platform that you are investigating, the PowerShell or the 
Python3 script may make more sense to run. In both cases, the optional 
argument is the top-level directory that you would like to use to begin your 
search.

Any file discovered is worth investigation to determine if the application 
using it is vulnerable. For any ``JndiLookup.class`` that is present, log4j 
commonly includes the version in the jar file name. For example, a hit on 
log4j-core-2.14.1.jar would be indicative of a vulnerable application. 
Alternatively, log4j-core-2.16.jar may also produce a hit because the 
JndiLookup code is still present in the 2.16 version of log4j, but it is 
disabled by default. See VU#930724 for more details.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

It is a partly feature complete tool by CERT Coordination Center 
(CERT/CC) [21]_ .

https://github.com/anchore/grype
##############################################################################

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A vulnerability scanner for container images and filesystems. Easily install 
the binary to try it out. Works with Syft, the powerful SBOM (software bill 
of materials) tool for container images and filesystems.

Features
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Scan the contents of a container image or filesystem to find known 
vulnerabilities.

Find vulnerabilities for major operating system packages:

- Alpine
- Amazon Linux
- BusyBox
- CentOS
- Debian
- Distroless
- Oracle Linux
- Red Hat (RHEL)
- Ubuntu

Find vulnerabilities for language-specific packages:

- Ruby (Gems)
- Java (JAR, WAR, EAR, JPI, HPI)
- JavaScript (NPM, Yarn)
- Python (Egg, Wheel, Poetry, requirements.txt/setup.py files)
- Supports Docker and OCI image formats

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some scanners did better than others, but none was able to
detect all formats.

Found 

- log4j versions
- Uber JAR
- Shaded JAR

not found

- Shaded JAR
- Compressed JAR (ZIP)
- Compressed archived JAR (GZIP)
- PAR file
- PAR file within a ZIP
- Compressed archived Uber JAR

https://github.com/jfrog/log4j-tools
##############################################################################

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

CVE-2021-44228 poses a serious threat to a wide range of Java-based 
applications.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some scanners did better than others, but none was able to
detect all formats.

Found 

- log4j versions
- Uber JAR
- Shaded JAR
- Shaded JAR
- Compressed JAR (ZIP)
- PAR file
- PAR file within a ZIP

not found

- Compressed archived JAR (GZIP)
- Compressed archived Uber JAR

https://github.com/CrowdStrike/CAST
##############################################################################

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This tool is a quick scanner to walk filesystems looking for vulnerable 
versions of log4j. Please see the blog post here [22]_ for more detailed 
discussion.

Currently, it scans a given set of directories for JAR, WAR, ZIP, or EAR 
files, then scans for files therein matching a known set of checksums.

- Be mindful of the resource consumption when running a scan to minimize the 
  impact on end-user systems.
- Intentionally allow a higher number of false-positive results, leaving the 
  decision in the hands of the system owners whether a given result warrants 
  further investigation. 
   - We may see higher false positives because we identify any trace of 
     vulnerable versions of Log4j, even if the vulnerability has been 
     addressed by removing one or more classes from the deployment.
- The results should be extremely reliable, as they`re based on cryptographic 
  checksums.
- Allow use of the tool with pre-indexed (e.g., "locate") file systems to 
  avoid scanning and  simply pass the paths to known files on the command line. 
   - For example, locate -0 *.jar | xargs -0 ./cast
- Provide the ability to tune memory usage - for example: 
   -recursion 0 to disable scanning sub-archives 
   -recursion 1 to scan only 1 sub-archive deep  
   -maxmem 1000000 to limit sub-archive scanning to 1MB (compressed)

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some scanners did better than others, but none was able to
detect all formats.

Found 

- log4j versions
- Uber JAR
- Shaded JAR
- Shaded JAR
- Compressed JAR (ZIP)
- PAR file within a ZIP

not found

- Compressed archived JAR (GZIP)
- PAR file
- Compressed archived Uber JAR

https://github.com/JagarYousef/log4j-dork-scanner
##############################################################################

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is an auto script to search, scrape and scan for Apache Log4j 
CVE-2021-44228 affected files using Google dorks.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Interested side kick way to find suspicious pattern [23]_ and [24]_ .

https://github.com/Qualys/log4jscanwin
##############################################################################

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Log4jScanner.exe utility helps to detect CVE-2021-4104, CVE-2021-44228, 
CVE-2021-44832, CVE-2021-45046, and CVE-2021-45105 vulnerabilities. The 
utility will scan the entire hard drive(s) including archives (and nested 
JARs) for the Java class that indicates the Java application contains a 
vulnerable log4j library. The utility will output its results to a console.

Qualys has added a new QID (376160) that is designed to look for the results 
of this scan and mark the asset as vulnerable if the vulnerable log4j 
library was found.

Impression
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some scanners did better than others, but none was able to
detect all formats.

Found 

- log4j versions
- Uber JAR
- Shaded JAR
- Shaded JAR
- Compressed JAR (ZIP)

not found

- PAR file within a ZIP
- Compressed archived JAR (GZIP)
- PAR file
- Compressed archived Uber JAR

.....

.. Seealso::
   :ref:`Vulnerabilities - log4j - Advisories list here <Section_Vulnerabilities_-_log4j_-_Advisories>`

.....

.. Rubric:: Footnotes

.. [1]
   https://github.com/fullhunt/log4j-scan

.. [2]
   https://github.com/Neo23x0/log4shell-detector

.. [3]
   https://github.com/NorthwaveSecurity/log4jcheck

.. [4]
   https://github.com/hillu/local-log4j-vuln-scanner

.. [5]
   https://github.com/adilsoybali/Log4j-RCE-Scanner
   
.. [6]
   https://github.com/takito1812/log4j-detect

.. [7]
   https://github.com/Diverto/nse-log4shell

.. [8]
   https://github.com/mergebase/log4j-detector

.. [9]
   https://github.com/palantir/log4j-sniffer

.. [10]
   https://github.com/CERTCC/CVE-2021-44228_scanner

.. [11]
   https://github.com/anchore/grype

.. [12]
   https://github.com/jfrog/log4j-tools

.. [13]
   https://github.com/CrowdStrike/CAST

.. [14]
   https://github.com/JagarYousef/log4j-dork-scanner

.. [15]
   https://github.com/Qualys/log4jscanwin

.. [20]
   Apache Karaf Archive
   https://karaf.apache.org/archives

.. [21]
   https://vuls.cert.org/

.. [22]
   https://www.crowdstrike.com/blog/free-targeted-log4j-search-tool/

.. [23]
   https://www.boxpiper.com/posts/google-dork-list

.. [24]
   https://gbhackers.com/latest-google-dorks-list/
