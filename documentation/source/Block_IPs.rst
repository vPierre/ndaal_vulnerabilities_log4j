******************************************************************************
ndaal - Vulnerabilities - log4j - Block IPs
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Block_IPs:

ndaal - Vulnerabilities - log4j - Block IPs
------------------------------------------------------------------------------

big dump from known **j4log/j4shell** malicious ip adresses unique and sorted 
update if changes were made!

**Sources:**

- https://gist.github.com/gnremy/c546c7911d5f876f263309d7161a7217
- https://github.com/Akikazuu/Apache-Log4j-RCE-Attempt
- https://github.com/RedDrip7/Log4Shell_CVE-2021-44228_related_attacks_IOCs
- https://gist.github.com/ycamper/26e021a2b5974049d113738d51e7641d
- https://github.com/Malwar3Ninja/Exploitation-of-Log4j2-CVE-2021-44228/blob/main/Threatview.io-log4j2-IOC-list
- https://raw.githubusercontent.com/Azure/Azure-Sentinel/master/Sample%20Data/Feeds/Log4j_IOC_List.csv
- https://github.com/CronUp/Malware-IOCs/blob/main/2021-12-11_Log4Shell_Botnets
- https://gist.github.com/blotus/f87ed46718bfdc634c9081110d243166
- https://tweetfeed.live/search.html
- https://raw.githubusercontent.com/CriticalPathSecurity/Public-Intelligence-Feeds/master/log4j.txt
- https://raw.githubusercontent.com/CriticalPathSecurity/Zeek-Intelligence-Feeds/master/log4j_ip.intel
- https://raw.githubusercontent.com/CriticalPathSecurity/Public-Intelligence-Feeds/master/log4j.txt
- https://urlhaus.abuse.ch/browse/tag/log4j/
- https://threatfox.abuse.ch/browse/tag/CVE-2021-44228/ ( and log4j)

the aggregated txt file with the **IPs** are under 

``../documention/source/_IPs/ips.txt`` 

you can use this list to block these endpoints with your firewalls.

.. Seealso::
   https://gitlab.com/vPierre/ndaal_vulnerabilities_log4j/-/blob/main/documentation/source/_IPs/ips.txt

.....

.. Seealso::
   :ref:`Vulnerabilities - log4j - Advisories list here <Section_Vulnerabilities_-_log4j_-_Advisories>`



.....

.. Rubric:: Footnotes

.. [1]
   - https://nvd.nist.gov/vuln/detail/CVE-2021-44228
