******************************************************************************
Document Information
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

.. contents:: Content
   :depth: 3

.. _Document_Information:

Summary
------------------------------------------------------------------------------

========================================  =====================================================
**Effective Date**: 17. November 2022     **Document Owner**: Pierre Gronau
========================================  =====================================================

========================================  =====================================================
**Next Review**: 17. November 2023        **Document Owner**: Pierre Gronau
========================================  =====================================================

..

Introduction Semantic Versioning Scheme
------------------------------------------------------------------------------

We are offering for your convenience a bilingual version of 
**Introduction Semantic Versioning Scheme**:

Under the SemVer Versioning Scheme [1]_ , our versioning would take on the 
following format:

.. image:: 
   _static/SemVer.png
   :width: 75%
   :align: center

Where **x**, **y**, and **z** are integers that increase numerically and
indicate **Major**, **Minor**, and **Patch**, respectively.

The SemVer specification assumes that projects using this scheme MUST have a 
public API. Based on this, let’s take a look at each part in more detail 
starting from left to right.

Major
##############################################################################

**The major version should increase when we’ve introduced new functionality 
which breaks our API, i.e., increase this value when we’ve added a 
backward-incompatible change to our project**. When this number is increased,
we must reset the Minor and Patch numbers to ``0``.

For example, if we have a project that is on version ``1.2.5`` and we have 
introduced a breaking change under the SemVer scheme, we must set our new 
version number to ``2.0.0``.

Minor
##############################################################################

**We should increase our minor version when we’ve introduced new functionality 
which changes our API but is backward compatible, i.e., a non-breaking change**. 
We can also opt to change the Minor version if we’ve made substantial changes
to the internal code of our project.

Similarly, when we change the Minor version we should also reset the patch 
version to ``0``. For example, updating the Minor version of a project at 
``2.0.1`` would set it to ``2.1.0``.

Patch
##############################################################################

**Under the SemVer specs, we reserve patch changes for backward-compatible
bug fixes**. A patch change should not involve any changes to the API.

Pre-Release and Build
##############################################################################

After the patch version, there are some optional labels we can add to our
versions, such as a pre-release label or a build number.

For example, to mark a package as a pre-release, we must add a hyphen then
the pre-release label, which can be a dot-separated identifier, e.g., 
``1.0.0-alpha.1`` tells us that this project is a pre-release version of 
``1.0.0 labeled alpha.1``. A pre-release label indicates that this version is 
unstable and has a high risk if we use it. When considering version 
precedence, a pre-release version is always of lower precedence than the
normal version.

If we want to indicate the build of that release, we can add a dot-separated
identifier of the build appended after the patch (or pre-release) with a 
``+`` sign. For example, ``1.0.0-alpha.1+001``. Build meta-data does not factor 
in precedence, so we can consider two versions that only differ in build 
number to be of the same precedence.

Development SemVer
##############################################################################

The SemVer specification reserves the Major version ``0.x.y`` for development
purposes. 
When starting a new project, it makes sense to start initial development at a 
release ``0.1.0`` since it will, of course, include features. SemVer assumes
a development version to be unstable and can change at any time.

.. Tip::
   We will use **SemVer** Versioning Scheme [1]_ under 
   the Attribution 3.0 Unported (CC BY 3.0) License [2]_.

Document Information
------------------------------------------------------------------------------

The Summary of the Document Information is listed here:

========================================  =====================================================
**Effective Date**: 17. November 2022    **Document Owner**: Pierre Gronau
========================================  =====================================================

========================================  =====================================================
**Next Review**: 17. November 2023       **Document Owner**: Pierre Gronau
========================================  =====================================================

..

Document History
##############################################################################

The whole Document History is listed here:

..

.. csv-table:: Document History
   :class: longtable
   :file: doc_history.csv
   :widths: 15,15,35,35
   :header-rows: 1


General Equal Treatment Act
##############################################################################

Consideration of the General Equal Treatment Act (AGG = Allgemeine 
Gleichbehandlungsgesetz). For reasons of practical readability, the following 
personal forms of address are limited to the masculine form of address; 
nevertheless, the information refers to members of all genders, i.e. male, 
female and diverse.

Validity
##############################################################################

The document must be adapted whenever a significant technical change is made. 
However, at the latest one year after the last review. It is advisable to 
review and adapt this document every six months.

Target group - Qualified personnel
##############################################################################

The product/system described in this documentation MAY only be handled by 
qualified personnel for the respective task in compliance with the 
observing the documentation pertaining to the respective task, 
in particular the safety instructions and warnings contained therein. 
Qualified personnel are, on the basis of their training and experience 
to recognize risks in the handling of these products/systems and to avoid 
possible hazards.

..

Typically, these are IT administrators, IT security experts, 
Automation experts (e.g. Ansible, Terraform und Pulumi), 
employees from data protection.

.....

.. Rubric:: Footnotes

.. [1]
   https://semver.org/spec/v2.0.0.html

.. [2]
   https://creativecommons.org/licenses/by/3.0/
