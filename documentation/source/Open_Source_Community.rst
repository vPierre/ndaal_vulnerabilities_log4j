******************************************************************************
ndaal - Vulnerabilities - log4j - Open Source Community
******************************************************************************

.. sectionauthor:: Frank Mördel <Frank.Moerdel@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Open_Source_Community:

The Open Source Community will fix it, once again?!
------------------------------------------------------------------------------

The probably most important Java repository (Maven Central Repository [2]_ ) 
was scanned by the Open Source team at **Google**. 
It turned out that more than **35,000 Java packages** in the repository are 
affected by the just disclosed **log4j** vulnerability.
If you consider that this is only 8% of all packages in percentage, you 
could lean back, couldn't you?

Far from it! James Wetter and Nicky Ringland described it with the word 
"**enormous**". The good news, however, is that the Open Source Community 
has already patched nearly 5,000 of the more than 35,000 packages since the 
vulnerability became known. This accounts for a good 13% of all packages 
marked vulnerable.

   .. Admonition:: Quote by J. Wetter and N. Ringland
      :class: Warning

      "This represents a rapid response and a mammoth effort by both log4j 
      maintainers and the broad Community of Open Source users" .

However, we all agree that the **log4j** problem will not be solved within the 
next days / weeks. It will keep us busy for years to come.

Why is that and why is it so hard to patch?
##############################################################################

Let's "briefly" look at the dependencies. **Log4j** is a framework for 
logging application messages. This is exactly where the difficulty lies, 
a **framewok**.
Wetter and Ringland aptly call it artifacts in 3'rd party applications. Thus 
**log4j** is not always to be seen in direct dependence to the Java packages, 
but is also a dependency of another / further dependency. This can also be 
called indirect dependency.

What exactly is meant by this?
##############################################################################

In the situation developers of 3'rd party applications have to wait for the 
developers of the actual application and can only then start with the actual 
work on the product, the patching.
Thus, updating their "own" application takes longer, we are talking about 
weeks and months in some circumstances.
Let's think about the quickly available first patch to Version 2.15. The work 
had just started, when the developers had to start all over again, at best 
they hadn't started working yet.
The patch to v2.15 had it "in it", or not, as we know.

.. figure:: 
   _static/blocked_dep_fix.png
   :alt: Blocked waiting for updated dependency
   :width: 50%
   :align: center

According to Google's report, however, **log4j** is "only" in about 
**7,000 packages** in a direct dependency. According to the current state, a 
Java package is considered secure if it uses **log4j** in version >= 2.17.0.
The additional work to be done by Java developers is to update the indirect 
dependencies.

Thanks go to the Open Source Community and maintainers. Once again it 
shows the strength of the Community to detect, analyze and fix 
vulnerabilities.

.. Rubric:: ...and what about patching?

It's good for the one who looks after a well-functioning IT. Automated IT 
operations have a clear advantage here. Affected machines can be quickly 
identified and eliminated.
To do this, the affected Service (bare metal, virtual machine / container / 
pod) is patched from the "source of truth", the running Service is discarded 
and the patched Service is rolled out automatically.
This is only to give a rough overview, but not to completely rob the admin of 
his Christmas vacation.

.. Rubric:: ...happy patching


.....

.. Rubric:: Footnotes

.. [1]
   https://security.googleblog.com/2021/12/understanding-impact-of-apache-log4j.html

.. [2]
   https://www.maven.org
