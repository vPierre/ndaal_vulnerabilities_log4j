******************************************************************************
ndaal - Vulnerabilities - log4j - Overview
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Overview:

Summary
------------------------------------------------------------------------------

``log4shell``, zero-day exploit in the popular Java logging library log4j2 was 
discovered that results in **remote code execution** (RCE) by logging a 
certain string.

Log4j2 is an open-source,, Java-based logging framework commonly incorporated
into "Software based on Java EE". "Apache web servers" and "Spring-Boot web 
applications".

The vulnerability has been reported with CVE-2021-44228 [1]_ against the 
log4j-core.jar. CVE-2021-44228 is considered a critical flaw, and it has a 
base CVSS score of 10, **the highest possible severity rating**.

.. figure:: 
   _static/log4j_attack.png
   :alt: The log4j JNDI Attack 
   :width: 75%
   :align: center

Who is Impacted!!
------------------------------------------------------------------------------

Too many services are vulnerable to this exploit as **log4j** is a wild rang
used Java-based logging utility. Cloud services like Stream, Apple iCloud, 
Office 365, and applications like Minecraft have already been found to be 
vulnerable.

Anybody using Apache framwworks services or any Spring-Boot Java-based 
framework applications uses log4j2 is likely to be vlnerable.

Also Internet of Things IT components, e.g. **SCADA** and **DCS** systems are
vulnerable

How the exploit works!!
------------------------------------------------------------------------------

Exploit Requirements
------------------------------------------------------------------------------

- A server with a vulnerable log4j version <= 2.16. [1]_ and [2]_ 
- An endpoint with any protocol (http, tcp, etc.) that allows an attacker to 
  send the exploit string. 

  .. Note:
     An Internet exposition is not needed. This weakness is also feasable for
     air-gapped environments.

- Log statement that logs out the string from the request even just a file on
  a file system.

Exploit Steps
------------------------------------------------------------------------------

- Data from the user gets sent to the server (via any protocol)
- The server logs the data in the request, containing the malicious payload.
- The log4j vulnerability is triggered by this payload and the server makes a 
  request to attacker.com via (JNDI).
- The response contains a patch to a remote Java class file which is injected
  into the server process.
- This injected payload triggers a second stage, and allows an attacker to 
  execute arbitrary code.

How to mitigate 
------------------------------------------------------------------------------

- Spot vulnerable applications
  run a search/grep command on all servers to spot any file with name 
  **log4j2**. Then check it is a vulnerable version or not.
- Permanent mitigation
  Version 2.17 or higher of log4j has been released without the vulnerability 
  as we know. Log4j-core.ja is available on Apache log4j page below. You can 
  download it and updated on your system. [3]_ , [7]_ 
- Temporary mitigation
   - add ``log4j2.formatMsgNoLookups=True`` to the global configuration of your
     server/web applications.
   - ``com.sun.jndi.rmi.object.trustURLCodebase`` and 
     ``com.sun.jndi.cosnaming.object.trustURLCodebase`` set to false

    ..

     **Other countermeasures such as disabling lookups log4j2.formatMsgNoLookups)**
     **or setting trustURLCodebase to false do not completely mitigate the** 
     **vulnerability as some attack vectors may remain open.**
   - If the JndiLookup.class file is located directly in the found file 
     (this can be a zip, jar, war, ear or aar file), it can be deleted 
     directly from this file:
     ``zip -q -d log4j-core-*.jar org/apache/logging/log4j/core/lookup/JndiLookup.class``
   - block and record IPs now using this vulnerability [4]_ 
   - activate a web application firewall like mod security [5]_ to protect web servers [6]_ 
     ModSecurity is an open source, cross platform web application firewall (WAF) engine for Apache, IIS and Nginx.
- Investigations

  ..

  Any relevant logs collected, to include, but not exclusive to:

  - Network logs
  - Endpoint Detection logs
  - IDS/IPS logs
  - Suspicious outbound connections
  - Apache logs

Timeline
------------------------------------------------------------------------------

Below is a timeline of the discovery of **Log4Shell** and its effects:

- 2021, December 28: The flaw CVE-2021-44832 [9]_ was detected and a new patch to 2.17.1 was needed.
- 2021, December 16: The flaw CVE-2021-45105 [7]_ was detected and a new patch to 2.17 was needed.
- 2021, December 14: 
    - CVE-2021-44228 in Apache Log4j 2.15.0 was incomplete in certain non-default configurations. Therefore CVE-2021-45046 [2]_ creates the need to patch to 2.16
    - Apache has released version 2.16.0, which completely removes support for Message Lookups and disables JNDI by default.
- 2021, December 13: The flaw CVE-2021-4104 [10]_ was detected
- 2021, December 10: Mass scanning and exploitation attempts of the vulnerability are recorded (see: Greynoise analysis)
- 2021, December 10: LunaSec publishes an analysis and detailed advisory of the vulnerability
- 2021, December 6:
    - ``allowedJndiProtocols`` restricts JNDI protocols to those listed; default: none
    - ``allowedLdapClasses`` lists names of allowed remote Java classes; default
    - ``allowedLdapHosts`` restricts LDAP requests to listed hosts; default: none
- 2021, November 30: The commit fixing the vulnerability is pushed to the Log4j codebase
- 2021, November 29: An issue, LOG4J2-3198, is created to fix the vulnerability
- 2021, November 26: MITRE assigns the CVE identifier CVE-2021-44228 [1]_ 
- 2019, October 14: CVE-2019-17571 CVSSv3: 9.8 CRITICAL log4j 1.2.4 <= 1.2.17 Vulnerability gets reportet [8]_ 

.....

.. Seealso::
   :ref:`Vulnerabilities - log4j - Advisories list here <Section_Vulnerabilities_-_log4j_-_Advisories>`



.....

.. Rubric:: Footnotes

.. [1]
   CVE-2021-44228
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228

.. [2]
   CVE-2021-45046
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046

.. [3]
   https://logging.apache.org/log4j/2.x/download.html

.. [4]
   IP lists
   - black list
     https://gitlab.com/vPierre/ndaal_vulnerabilities_log4j/-/blob/main/documentation/source/_IPs/ips.txt
   - grey list
     https://gitlab.com/vPierre/ndaal_vulnerabilities_log4j/-/blob/main/documentation/source/_IPs/grey.txt
   - white list
     https://gitlab.com/vPierre/ndaal_vulnerabilities_log4j/-/blob/main/documentation/source/_IPs/white.txt

.. [5]
   https://owasp.org/www-project-modsecurity-core-rule-set/

.. [6]
   CRS and Log4j / Log4Shell / CVE-2021-44228
   https://coreruleset.org/20211213/crs-and-log4j-log4shell-cve-2021-44228/

.. [7]
   CVE-2021-45105
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105

.. [8]
   CVE-2019-17571
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-17571

.. [9]
   CVE-2021-44832
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832

.. [10]
   CVE-2021-4104
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-4104
