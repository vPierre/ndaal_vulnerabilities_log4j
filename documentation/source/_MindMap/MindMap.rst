Infosec Mind Maps
==============================================================================

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Java_Packages:

3 mindmaps designed to help mitigating / protecting against the Log4shell vulnerability (lots of CVEs now...) :

- Mind map #1 : Decision tree to identify if the code in-use is vulnerable
- Mind map #2 : How to detect the vulnerability, from the black box or 
  white box perspective
- Mind map #3 : Shielding & mitigations against Log4shell : Patching is one 
  thing, but defence in depth is advised. A few pointers in this mind map can 
  help. [1]_ 

Decision tree to identify if the code in-use is vulnerable
##############################################################################

.. figure:: 
   AmIVulnerable-Log4shell-v6.1.1.png
   :alt: Decision tree to identify if the code in-use is vulnerable
   :width: 100%
   :align: center

How to detect the vulnerability, from the black box or white box perspective
##############################################################################

.. figure:: 
   v2-detectLog4shell.png
   :alt: How to detect the vulnerability, from the black box or white box perspective
   :width: 100%
   :align: center

Shielding & Mitigations against Log4shell
##############################################################################

Patching is one thing, but defence in depth is advised. A few pointers in this 
mind map can help. [1]_ 

.. figure:: 
   Shield-Log4shell-v1.png
   :alt: Shielding & Mitigations against Log4shell
   :width: 100%
   :align: center


.....

.. Rubric:: Footnotes

.. [1]
   https://github.com/DickReverse/InfosecMindmaps
