\selectlanguage *{english}
\contentsline {chapter}{\numberline {1}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Introduction}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}CVE\sphinxhyphen {}2021\sphinxhyphen {}44228}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}CVE\sphinxhyphen {}2021\sphinxhyphen {}45046}{3}{section.1.2}%
\contentsline {section}{\numberline {1.3}CVE\sphinxhyphen {}2021\sphinxhyphen {}45105}{3}{section.1.3}%
\contentsline {section}{\numberline {1.4}CVE\sphinxhyphen {}2021\sphinxhyphen {}44832}{3}{section.1.4}%
\contentsline {section}{\numberline {1.5}CVE\sphinxhyphen {}2021\sphinxhyphen {}4104}{4}{section.1.5}%
\contentsline {section}{\numberline {1.6}Patch}{4}{section.1.6}%
\contentsline {section}{\numberline {1.7}Documentation}{4}{section.1.7}%
\contentsline {section}{\numberline {1.8}Other \sphinxhyphen {} similar attack pattern}{4}{section.1.8}%
\contentsline {chapter}{\numberline {2}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Overview}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}Summary}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}Who is Impacted!!}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}How the exploit works!!}{7}{section.2.3}%
\contentsline {section}{\numberline {2.4}Exploit Requirements}{7}{section.2.4}%
\contentsline {section}{\numberline {2.5}Exploit Steps}{7}{section.2.5}%
\contentsline {section}{\numberline {2.6}How to mitigate}{8}{section.2.6}%
\contentsline {section}{\numberline {2.7}Timeline}{9}{section.2.7}%
\contentsline {chapter}{\numberline {3}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Check}{10}{chapter.3}%
\contentsline {section}{\numberline {3.1}What are the Limitations of Binary Based Identification?}{10}{section.3.1}%
\contentsline {section}{\numberline {3.2}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Check}{11}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}https://github.com/fullhunt/log4j\sphinxhyphen {}scan}{12}{subsection.3.2.1}%
\contentsline {subsubsection}{Features}{12}{subsubsection*.74}%
\contentsline {subsubsection}{Description}{12}{subsubsection*.75}%
\contentsline {subsubsection}{Impression}{12}{subsubsection*.76}%
\contentsline {subsection}{\numberline {3.2.2}https://github.com/Neo23x0/log4shell\sphinxhyphen {}detector}{12}{subsection.3.2.2}%
\contentsline {subsubsection}{Features}{12}{subsubsection*.77}%
\contentsline {subsubsection}{Description}{13}{subsubsection*.78}%
\contentsline {subsubsection}{Impression}{13}{subsubsection*.79}%
\contentsline {subsection}{\numberline {3.2.3}https://github.com/NorthwaveSecurity/log4jcheck}{13}{subsection.3.2.3}%
\contentsline {subsubsection}{Features}{13}{subsubsection*.80}%
\contentsline {subsubsection}{Description}{13}{subsubsection*.81}%
\contentsline {subsubsection}{Impression}{14}{subsubsection*.82}%
\contentsline {subsection}{\numberline {3.2.4}https://github.com/hillu/local\sphinxhyphen {}log4j\sphinxhyphen {}vuln\sphinxhyphen {}scanner}{14}{subsection.3.2.4}%
\contentsline {subsubsection}{Features}{14}{subsubsection*.83}%
\contentsline {subsubsection}{Description}{14}{subsubsection*.84}%
\contentsline {subsubsection}{Impression}{14}{subsubsection*.85}%
\contentsline {subsection}{\numberline {3.2.5}https://github.com/adilsoybali/Log4j\sphinxhyphen {}RCE\sphinxhyphen {}Scanner}{14}{subsection.3.2.5}%
\contentsline {subsubsection}{Features}{14}{subsubsection*.86}%
\contentsline {subsubsection}{Description}{14}{subsubsection*.87}%
\contentsline {subsubsection}{Impression}{14}{subsubsection*.88}%
\contentsline {subsection}{\numberline {3.2.6}https://github.com/takito1812/log4j\sphinxhyphen {}detect}{14}{subsection.3.2.6}%
\contentsline {subsubsection}{Features}{14}{subsubsection*.89}%
\contentsline {subsubsection}{Impression}{15}{subsubsection*.90}%
\contentsline {subsection}{\numberline {3.2.7}https://github.com/Diverto/nse\sphinxhyphen {}log4shell}{15}{subsection.3.2.7}%
\contentsline {subsubsection}{Features}{15}{subsubsection*.91}%
\contentsline {subsubsection}{Impression}{15}{subsubsection*.92}%
\contentsline {subsection}{\numberline {3.2.8}https://github.com/mergebase/log4j\sphinxhyphen {}detector}{15}{subsection.3.2.8}%
\contentsline {subsubsection}{Features}{15}{subsubsection*.93}%
\contentsline {subsubsection}{Impression}{15}{subsubsection*.94}%
\contentsline {subsection}{\numberline {3.2.9}https://github.com/palantir/log4j\sphinxhyphen {}sniffer}{16}{subsection.3.2.9}%
\contentsline {subsubsection}{Features}{16}{subsubsection*.95}%
\contentsline {subsubsection}{Impression}{16}{subsubsection*.96}%
\contentsline {subsection}{\numberline {3.2.10}https://github.com/CERTCC/CVE\sphinxhyphen {}2021\sphinxhyphen {}44228\_scanner}{17}{subsection.3.2.10}%
\contentsline {subsubsection}{Features}{17}{subsubsection*.97}%
\contentsline {subsubsection}{Impression}{17}{subsubsection*.98}%
\contentsline {subsection}{\numberline {3.2.11}https://github.com/anchore/grype}{17}{subsection.3.2.11}%
\contentsline {subsubsection}{Description}{17}{subsubsection*.100}%
\contentsline {subsubsection}{Features}{18}{subsubsection*.101}%
\contentsline {subsubsection}{Impression}{18}{subsubsection*.102}%
\contentsline {subsection}{\numberline {3.2.12}https://github.com/jfrog/log4j\sphinxhyphen {}tools}{19}{subsection.3.2.12}%
\contentsline {subsubsection}{Description}{19}{subsubsection*.103}%
\contentsline {subsubsection}{Impression}{19}{subsubsection*.104}%
\contentsline {subsection}{\numberline {3.2.13}https://github.com/CrowdStrike/CAST}{19}{subsection.3.2.13}%
\contentsline {subsubsection}{Description}{19}{subsubsection*.105}%
\contentsline {subsubsection}{Impression}{20}{subsubsection*.107}%
\contentsline {subsection}{\numberline {3.2.14}https://github.com/JagarYousef/log4j\sphinxhyphen {}dork\sphinxhyphen {}scanner}{20}{subsection.3.2.14}%
\contentsline {subsubsection}{Description}{20}{subsubsection*.108}%
\contentsline {subsubsection}{Impression}{20}{subsubsection*.109}%
\contentsline {subsection}{\numberline {3.2.15}https://github.com/Qualys/log4jscanwin}{20}{subsection.3.2.15}%
\contentsline {subsubsection}{Description}{20}{subsubsection*.112}%
\contentsline {subsubsection}{Impression}{21}{subsubsection*.113}%
\contentsline {chapter}{\numberline {4}Automation Solution with Debian Linux for Linux and Windows}{22}{chapter.4}%
\contentsline {section}{\numberline {4.1}log4shell\sphinxhyphen {}detector}{22}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Running the log4shell\sphinxhyphen {}detector}{23}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Log4shell\sphinxhyphen {}detector semantic logic}{26}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Extracting log file information with automated tools}{26}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Tool 1: Extracting exploited and unprocessed files}{26}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Tool 2: Extract and detect black/grey listed IP addresses}{27}{subsection.4.2.2}%
\contentsline {section}{\numberline {4.3}What are the Limitations of Binary Based Identification?}{28}{section.4.3}%
\contentsline {section}{\numberline {4.4}Other}{29}{section.4.4}%
\contentsline {section}{\numberline {4.5}log4j\sphinxhyphen {}sniffer}{29}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Installing the log4j\sphinxhyphen {}sniffer}{30}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}log4j\sphinxhyphen {}sniffer on Linux}{30}{subsection.4.5.2}%
\contentsline {subsection}{\numberline {4.5.3}log4j\sphinxhyphen {}sniffer on mac}{31}{subsection.4.5.3}%
\contentsline {subsection}{\numberline {4.5.4}log4j\sphinxhyphen {}sniffer on Windows}{31}{subsection.4.5.4}%
\contentsline {subsection}{\numberline {4.5.5}log4j\sphinxhyphen {}sniffer primary usage}{32}{subsection.4.5.5}%
\contentsline {subsection}{\numberline {4.5.6}log4j\sphinxhyphen {}sniffer semantic logic}{32}{subsection.4.5.6}%
\contentsline {chapter}{\numberline {5}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Block IPs}{35}{chapter.5}%
\contentsline {section}{\numberline {5.1}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Block IPs}{35}{section.5.1}%
\contentsline {chapter}{\numberline {6}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Open Source Community}{37}{chapter.6}%
\contentsline {section}{\numberline {6.1}The Open Source Community will fix it, once again?!}{37}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Why is that and why is it so hard to patch?}{37}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}What exactly is meant by this?}{38}{subsection.6.1.2}%
\contentsline {chapter}{\numberline {7}Infosec Mind Maps}{39}{chapter.7}%
\contentsline {section}{\numberline {7.1}Decision tree to identify if the code in\sphinxhyphen {}use is vulnerable}{39}{section.7.1}%
\contentsline {section}{\numberline {7.2}How to detect the vulnerability, from the black box or white box perspective}{40}{section.7.2}%
\contentsline {section}{\numberline {7.3}Shielding \& Mitigations against Log4shell}{40}{section.7.3}%
\contentsline {chapter}{\numberline {8}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Advisories}{42}{chapter.8}%
\contentsline {section}{\numberline {8.1}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Advisories}{43}{section.8.1}%
\contentsline {subsection}{\numberline {8.1.1}0\sphinxhyphen {}9}{43}{subsection.8.1.1}%
\contentsline {subsection}{\numberline {8.1.2}A}{43}{subsection.8.1.2}%
\contentsline {subsection}{\numberline {8.1.3}B}{47}{subsection.8.1.3}%
\contentsline {subsection}{\numberline {8.1.4}C}{55}{subsection.8.1.4}%
\contentsline {subsection}{\numberline {8.1.5}D}{58}{subsection.8.1.5}%
\contentsline {subsection}{\numberline {8.1.6}E}{59}{subsection.8.1.6}%
\contentsline {subsection}{\numberline {8.1.7}F}{60}{subsection.8.1.7}%
\contentsline {subsection}{\numberline {8.1.8}G}{61}{subsection.8.1.8}%
\contentsline {subsection}{\numberline {8.1.9}H}{62}{subsection.8.1.9}%
\contentsline {subsection}{\numberline {8.1.10}I}{63}{subsection.8.1.10}%
\contentsline {subsection}{\numberline {8.1.11}J}{65}{subsection.8.1.11}%
\contentsline {subsection}{\numberline {8.1.12}K}{66}{subsection.8.1.12}%
\contentsline {subsection}{\numberline {8.1.13}L}{67}{subsection.8.1.13}%
\contentsline {subsection}{\numberline {8.1.14}M}{68}{subsection.8.1.14}%
\contentsline {subsection}{\numberline {8.1.15}N}{70}{subsection.8.1.15}%
\contentsline {subsection}{\numberline {8.1.16}O}{71}{subsection.8.1.16}%
\contentsline {subsection}{\numberline {8.1.17}P}{72}{subsection.8.1.17}%
\contentsline {subsection}{\numberline {8.1.18}Q}{73}{subsection.8.1.18}%
\contentsline {subsection}{\numberline {8.1.19}R}{74}{subsection.8.1.19}%
\contentsline {subsection}{\numberline {8.1.20}S}{75}{subsection.8.1.20}%
\contentsline {subsection}{\numberline {8.1.21}T}{79}{subsection.8.1.21}%
\contentsline {subsection}{\numberline {8.1.22}U}{80}{subsection.8.1.22}%
\contentsline {subsection}{\numberline {8.1.23}V}{80}{subsection.8.1.23}%
\contentsline {subsection}{\numberline {8.1.24}W}{81}{subsection.8.1.24}%
\contentsline {subsection}{\numberline {8.1.25}X}{82}{subsection.8.1.25}%
\contentsline {subsection}{\numberline {8.1.26}Y}{83}{subsection.8.1.26}%
\contentsline {subsection}{\numberline {8.1.27}Z}{83}{subsection.8.1.27}%
\contentsline {section}{\numberline {8.2}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Other great resources}{84}{section.8.2}%
\contentsline {chapter}{\numberline {9}Pattern}{85}{chapter.9}%
\contentsline {section}{\numberline {9.1}log4j \sphinxhyphen {} Search Patterns}{86}{section.9.1}%
\contentsline {section}{\numberline {9.2}log4j \sphinxhyphen {} Callback Domains}{88}{section.9.2}%
\contentsline {chapter}{\numberline {10}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Hashes}{220}{chapter.10}%
\contentsline {section}{\numberline {10.1}Checkums by Apache}{220}{section.10.1}%
\contentsline {subsection}{\numberline {10.1.1}md5sum by Apache}{220}{subsection.10.1.1}%
\contentsline {subsection}{\numberline {10.1.2}sha1sum by Apache}{226}{subsection.10.1.2}%
\contentsline {subsection}{\numberline {10.1.3}sha256sum by Apache}{231}{subsection.10.1.3}%
\contentsline {subsection}{\numberline {10.1.4}sha512sum by Apache}{238}{subsection.10.1.4}%
\contentsline {subsection}{\numberline {10.1.5}other topic}{241}{subsection.10.1.5}%
\contentsline {section}{\numberline {10.2}vulnerable log4j hashes by other sources}{241}{section.10.2}%
\contentsline {section}{\numberline {10.3}Evaluating if a log4j JAR is vulnerable}{241}{section.10.3}%
\contentsline {chapter}{\numberline {11}Java Packages}{242}{chapter.11}%
\contentsline {chapter}{\numberline {12}ndaal \sphinxhyphen {} Vulnerabilities \sphinxhyphen {} log4j \sphinxhyphen {} Germany \sphinxhyphen {} German}{274}{chapter.12}%
\contentsline {section}{\numberline {12.1}Zusammenfassung}{274}{section.12.1}%
\contentsline {section}{\numberline {12.2}Hessische Beauftragte für Datenschutz und Informationsfreiheit}{274}{section.12.2}%
\contentsline {section}{\numberline {12.3}BSI}{275}{section.12.3}%
\contentsline {section}{\numberline {12.4}Weiterführende Links}{276}{section.12.4}%
\contentsline {section}{\numberline {12.5}Hinweise}{276}{section.12.5}%
\contentsline {chapter}{\numberline {13}Document Information}{278}{chapter.13}%
\contentsline {section}{\numberline {13.1}Summary}{278}{section.13.1}%
\contentsline {section}{\numberline {13.2}Introduction Semantic Versioning Scheme}{278}{section.13.2}%
\contentsline {subsection}{\numberline {13.2.1}Major}{279}{subsection.13.2.1}%
\contentsline {subsection}{\numberline {13.2.2}Minor}{279}{subsection.13.2.2}%
\contentsline {subsection}{\numberline {13.2.3}Patch}{279}{subsection.13.2.3}%
\contentsline {subsection}{\numberline {13.2.4}Pre\sphinxhyphen {}Release and Build}{280}{subsection.13.2.4}%
\contentsline {subsection}{\numberline {13.2.5}Development SemVer}{280}{subsection.13.2.5}%
\contentsline {section}{\numberline {13.3}Document Information}{280}{section.13.3}%
\contentsline {subsection}{\numberline {13.3.1}Document History}{280}{subsection.13.3.1}%
\contentsline {subsection}{\numberline {13.3.2}General Equal Treatment Act}{290}{subsection.13.3.2}%
\contentsline {subsection}{\numberline {13.3.3}Validity}{290}{subsection.13.3.3}%
\contentsline {subsection}{\numberline {13.3.4}Target group \sphinxhyphen {} Qualified personnel}{290}{subsection.13.3.4}%
\contentsline {chapter}{\numberline {14}ndaal \sphinxhyphen {} General Information}{291}{chapter.14}%
\contentsline {section}{\numberline {14.1}General Information}{291}{section.14.1}%
\contentsline {section}{\numberline {14.2}Opening Clause}{291}{section.14.2}%
\contentsline {section}{\numberline {14.3}Disclaimer}{292}{section.14.3}%
\contentsline {section}{\numberline {14.4}Rights of Use}{292}{section.14.4}%
\contentsline {section}{\numberline {14.5}No duty to update}{292}{section.14.5}%
\contentsline {chapter}{Contact}{293}{section*.261}%
