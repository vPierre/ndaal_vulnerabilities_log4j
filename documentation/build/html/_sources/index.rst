.. raw:: latex

    \pagestyle{plain}{
    \lhead{}
    \fancyhead[R]{\includegraphics{Logo.png}
    \fancyhead[L]{}}
    \fancyfoot[C]{\textbf{Public}}}
    \fancyfoot[L]{\textbf{Chapter \thechapter}}

.. toctree::
   :numbered:
   :glob:
   :maxdepth: 3

   Introduction.rst
   Overview.rst
   Check.rst
   automation_solutions.rst
   Block_IPs.rst
   ;related_Software.rst
   Open_Source_Community.rst
   _MindMap/MindMap.rst
   Advisories.rst
   _pattern/Pattern.rst
   _hashes/Hashes.rst
   _Java/Packages.rst
   Germany.rst
   ;_FAQs/*
   ;glance.rst
   ;_abstract/abstract.rst
   _document/Document.rst
   _legal/Information.rst

.. toctree::
   :numbered:
   :glob:
   :maxdepth: 3

   ;_finalchapter/figs.rst
   ;_finalchapter/tables.rst
   ;_finalchapter/bib.rst
   _finalchapter/final.rst

.. raw:: latex

    \clearpage
    \pagenumbering{arabic}
