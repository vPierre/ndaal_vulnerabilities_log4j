******************************************************************************
ndaal - Vulnerabilities - log4j - Advisories
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Vulnerabilities_-_log4j_-_Advisories:

ndaal - Vulnerabilities - log4j - Advisories
------------------------------------------------------------------------------


.. raw:: latex

    \raggedright

An alphabetically list of Advisories from IT Vendors.

0-9
##############################################################################

- 1Password : https://1password.community/discussion/comment/622612/#Comment_622612
- 2Brightsparks : https://github.com/NCSC-NL/log4shell/blob/main/software/vendor-statements/2Brightsparks.png
- 2n : https://www.2n.com/cs_CZ/novinky/produkty-2n-neohrozuje-zranitelnost-cve-2021-44228-komponenty-log4j-2
- 2wcom : https://www.2wcom.com/2wcom-not-using-log4j/
- 3CX : https://www.3cx.com/community/threads/log4j-vulnerability-cve-2021-44228.86436/#post-407911
- 3HMIS : https://support.3mhis.com/app/account/updates/ri/5210
- 7-Zip : https://sourceforge.net/p/sevenzip/discussion/45797/thread/b977bbd4d1/
- 8x8 : https://support.8x8.com/support-services/support/Apache_Log4j_2_Vulnerability


A
##############################################################################

- A10 Networks : https://support.a10networks.com/support/security_advisory/log4j-cve-2021-44228-cve-2021-45046/
- Abbott : https://www.abbott.com/policies/cybersecurity/apache-Log4j.html
- ABB : 
      - https://search.abb.com/library/Download.aspx?DocumentID=9ADB012621&LanguageCode=en&DocumentPartId=&Action=Launch
      - ABB, B&R Products : https://www.br-automation.com/downloads_br_productcatalogue/assets/1639507581859-en-original-1.0.pdf
- Accellence : https://www.accellence.de/en/articles/national-vulnerability-database-62
- Accellion : https://www.kiteworks.com/kiteworks-news/log4shell-apache-vulnerability-what-kiteworks-customers-need-to-know/
- Acquia : https://support.acquia.com/hc/en-us/articles/4415823329047-Apache-log4j-CVE-2021-44228
- Acronis : https://security-advisory.acronis.com/advisories/SEC-3859
- ActiveState : https://www.activestate.com/blog/activestate-statement-java-log4j-vulnerability/
- Acunetix : https://www.acunetix.com/blog/web-security-zone/critical-alert-log4shell-cve-2021-44228-in-log4j-possibly-the-biggest-impact-vulnerability-ever/
- Adaptec : https://ask.adaptec.com/app/answers/detail/a_id/17523/kw/log4j
- Addigy : https://addigy.com/blog/addigy-and-apaches-log4j2-cve-2021-44228-status/
- Adeptia : https://support.adeptia.com/hc/en-us/articles/4412815509524-CVE-2021-44228-Log4j2-Vulnerability-Mitigation-
- Adobe ColdFusion : https://helpx.adobe.com/coldfusion/kb/log4j-vulnerability-coldfusion.html
- ADP : https://www.adp.com/about-adp/data-security/alerts/adp-vulnerability-statement-apache-log4j-vulnerability-cve-2021-44228.aspx
- Adva : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3996900
- AFAS Software : https://help.afas.nl/vraagantwoord/NL/SE/120439.htm
- AFHCAN Global LLC :
   - AFHCAN Global LLC, AFHCANsuite,8.0.7 - 8.4.3,Not Affected : https://afhcan.org/support.aspx](https://afhcan.org/support.aspx
   - AFHCAN Global LLC, AFHCANServer | 8.0.7 - 8.4.3 : https://afhcan.org/support.aspx](https://afhcan.org/support.aspx
   - AFHCAN Global LLC, AFHCANcart | 8.0.7 - 8.4.3 : https://afhcan.org/support.aspx](https://afhcan.org/support.aspx
   - AFHCAN Global LLC, AFHCANweb | 8.0.7 - 8.4.3 : https://afhcan.org/support.aspx](https://afhcan.org/support.aspx
   - AFHCAN Global LLC, AFHCANmobile | 8.0.7 - 8.4.3 : https://afhcan.org/support.aspx](https://afhcan.org/support.aspx
   - AFHCAN Global LLC, AFHCANupdate | 8.0.7 - 8.4.3 : https://afhcan.org/support.aspx](https://afhcan.org/support.aspx
- Agenda : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995221
- Agilysys : https://info.agilysys.com/webmail/76642/2001127877/c3fda575e2313fac1f6a203dc6fc1db2439c3db0da22bde1b6c1b6747d7f0e2f
- Akamai : https://www.akamai.com/blog/news/CVE-2021-44228-Zero-Day-Vulnerability
- Alexion : https://alexion.nl/blog/alexion-crm-niet-vatbaar-voor-log4shell
- Alcatel : https://dokuwiki.alu4u.com/doku.php?id=log4j
- Alertus : https://help.alertus.com/s/article/Security-Advisory-Log4Shell-Vulnerability?language=en_US
- Alfresco : https://hub.alfresco.com/t5/alfresco-content-services-blog/cve-2021-44228-related-to-apache-log4j-security-advisory/ba-p/310717
- AlgoSec UNOFICIAl : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3994072
- AlienVault : https://success.alienvault.com/s/article/are-USM-Anywhere-or-USM-Central-vulnerable-to-CVE-2021-44228
- Alphatron Medical : https://www.alphatronmedical.com/home.html
- Altaro : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995170
- Altium : https://www.altium.com/trust-faqs/log4shell-vulnerability
- AMI : https://www.ami.com/ami-analysis-and-response-to-cve-2021-44228-log4j-vulnerability/
- Anaqua : https://www.anaqua.com/
- APACHE Global : https://blogs.apache.org/security/entry/cve-2021-44228
- Apero CAS : https://apereo.github.io/2021/12/11/log4j-vuln/
- Apigee : https://status.apigee.com/incidents/3cgzb0q2r10p
- Apollo : https://community.apollographql.com/t/log4j-vulnerability/2214
- Appdynamics : https://docs.appdynamics.com/display/PAA/Security+Advisory%3A+Apache+Log4j+Vulnerability
- Appeon : https://community.appeon.com/index.php/qna/q-a/apache-log4j2-remote-code-execution-vulnerability-cve-2021-44228-and-powerbuilder-infomaker#reply-31358
- AppGate : https://www.appgate.com/blog/appgate-sdp-unaffected-by-log4j-vulnerability
- Apple : 
   - https://9to5mac.com/2021/12/14/apple-patches-log4shell-icloud-vulnerability/
   - https://developer.apple.com/forums/thread/696785
- AppviewX : https://www.appviewx.com/blogs/apache-log4j-cve-2021-44228-vulnerability-zero-trust-networks-are-the-future/
- APPSHEET : https://community.appsheet.com/t/appsheet-statement-on-log4j-vulnerability-cve-2021-44228/59976
- Aptible : https://status.aptible.com/incidents/gk1rh440h36s?u=zfbcrbt2lkv4
- Aqua Security : https://docs.google.com/document/d/e/2PACX-1vSmFR3oHPXOih1wENKd7RXn0dsHzgPUe91jJwDTsaVxJtcJEroktWNLq7BMUx9v7oDZRHqLVgkJnqCm/pub
- Arca Noae : https://www.arcanoae.com/apache-log4j-vulnerability-cve-2021-44228/
- Arduino : https://support.arduino.cc/hc/en-us/articles/4412377144338-Arduino-s-response-to-Log4j2-vulnerability-CVE-2021-44228
- Ariba : https://connectsupport.ariba.com/sites#announcements-display&/Event/908469
- Arista : 
   - https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
   - CloudVision Portal, >2019.1.0 : https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
   - CloudVision Wi-Fi, virtual appliance or physical appliance : https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
   - Analytics Node for DANZ Monitoring Fabric (formerly Big Monitoring Fabric >7.0.0 : https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
   - Analytics Node for Converged Cloud Fabric (formerly Big Cloud Fabric >7.0.0 : https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
   - Embedded Analytics for Converged Cloud Fabric (formerly Big Cloud Fabric >5.3.0 : https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
   - CloudVision Portal, >2019.1.0 : https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
   - CloudVision Wi-Fi, virtual appliance or physical appliance, >8.8 : https://www.arista.com/en/support/advisories-notices/security-advisories/13425-security-advisory-0070
- ArrayNetworks : https://twitter.com/ArraySupport/status/1470141638571745282
- ArcServe : https://twitter.com/Arcserve/status/1470571214263361537
- ArcticWolf : https://arcticwolf.com/resources/blog/log4j
- Aruba Networks: https://asp.arubanetworks.com/notifications/Tm90aWZpY2F0aW9uOjEwMTQ0;notificationCategory=Security
- AspenTech : https://esupport.aspentech.com/S_Article?id=000099310
- Ataccama : https://www.ataccama.com/files/log4j2-vulnerability-cve-2021-44228-fix.pdf
- Atempo : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3999431
- Atera : https://www.reddit.com/r/atera/comments/rh7xb1/apache_log4j_2_security_advisory_update/
- Attivo networks : https://www.attivonetworks.com/wp-content/uploads/2021/12/Log4j_Vulnerability-Advisory-211213-4.pdf
- Atlassian : https://confluence.atlassian.com/kb/faq-for-cve-2021-44228-1103069406.html 
- AudioCodes : https://services.audiocodes.com/app/answers/kbdetail/a_id/2225
- Autopsy : https://www.autopsy.com/autopsy-and-log4j-vulnerability/
- Auth0 : https://twitter.com/auth0/status/1470086301902721024
- Autodesk : https://knowledge.autodesk.com/support/autocad/troubleshooting/caas/sfdcarticles/sfdcarticles/CVE-2021-44228.html
- Automox : https://blog.automox.com/log4j-critical-vulnerability-scores-a-10
- Auvik : https://status.auvik.com/incidents/58bfngkz69mj
- Avantra SYSLINK : https://support.avantra.com/support/solutions/articles/44002291388-cve-2021-44228-log4j-2-vulnerability
- Avaya : https://support.avaya.com/helpcenter/getGenericDetails?detailId=1399839287609
- AVEPOINT : https://www.avepoint.com/company/java-zero-day-vulnerability-notification
- AVM : https://avm.de/service/aktuelle-sicherheitshinweise/#Schwachstelle%20im%20Java-Projekt%20%E2%80%9Elog4j%E2%80%9C
- AvTech RoomAlert : https://avtech.com/articles/23124/java-exploit-room-alert-link/
- AWS New : https://aws.amazon.com/security/security-bulletins/AWS-2021-006/
- AWS OLD: https://aws.amazon.com/security/security-bulletins/AWS-2021-005/
- AXS Guard : https://www.axsguard.com/en_US/blog/security-news-4/log4j-vulnerability-77
- Axway Applications : https://support.axway.com/news/1331/lang/en
- AXON : https://my.axon.com/s/trust/response-to-log4j2-vuln?language=en_US
- AxxonSoft : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3996387
- AZURE Datalake store java : https://github.com/Azure/azure-data-lake-store-java/blob/ed5d6304783286c3cfff0a1dee457a922e23ad48/CHANGES.md#version-2310

B
##############################################################################

- B&W Software : https://www.buw-soft.de/en/2021/12/20/investigation-about-log4j-vulnerability-of-bw-products/
- BackBox : https://updates.backbox.com/V6.5/Docs/CVE-2021-44228.pdf
- BACKBLAZE : https://twitter.com/backblaze/status/1469477224277368838
- Balbix : https://www.balbix.com/blog/broad-exposure-to-log4shell-cve-2021-44228-highlights-how-the-attack-surface-has-exploded/
- Baramundi Products : https://forum.baramundi.com/index.php?threads/baramundi-produkte-von-log4shell-schwachstelle-in-log4j-nicht-betroffen.12539/#post-62875
- Barco : https://www.barco.com/en/support/knowledge-base/kb12495
- Barracuda : https://www.barracuda.com/company/legal/trust-center
- Baxter : https://www.baxter.com/sites/g/files/ebysai746/files/2021-12/Apache_Log4j_Vulnerability.pdf
- BBraun : https://www.bbraun.com/en/products-and-therapies/services/b-braun-vulnerability-disclosure-policy/security-advisory/b-braun-statement-on-Apache_Log4j.html
- BD : 
   - https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD Arctic Sun Analytics : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD Diabetes Care App Cloud : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD HealthSight Clinical Advisor : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD HealthSight Data Manager : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD HealthSight Diversion Management : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD HealthSight Infection Advisor : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD HealthSight Inventory Optimization Analytics : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD HealthSight Medication Safety : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD Knowledge Portal for Infusion Technologies : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD Knowledge Portal for Medication Technologies: https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD Knowledge Portal for BD Pyxis Supply : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD Synapsys Informatics Solution : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
   - BD Veritor COVID At Home Solution Cloud : https://cybersecurity.bd.com/bulletins-and-patches/third-party-vulnerability-apache-log4j
- BEC Legal Systems : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995524
- Beckman Coulter : https://www.beckmancoulter.com/en/about-beckman-coulter/product-security/product-security-updates
- Bender : https://www.bender.de/en/cert
- Best Practical Request Tracker (RT) and Request Tracker for Incident Response (RTIR) : https://bestpractical.com/blog/2021/12/request-tracker-rt-and-request-tracker-for-incident-response-rtir-do-not-use-log4j
- BeyondTrust Bomgar : https://beyondtrustcorp.service-now.com/kb_view.do?sysparm_article=KB0016542
- BigBlueButton : https://github.com/bigbluebutton/bigbluebutton/issues/13897#issuecomment-991652632
- BisectHosting : https://www.bisecthosting.com/clients/index.php?rp=/knowledgebase/205/Java-Log4j-Vulnerability.html
- BitDefender : https://businessinsights.bitdefender.com/security-advisory-bitdefender-response-to-critical-0-day-apache-log4j2-vulnerability
- BitNami By VMware : https://docs.bitnami.com/general/security/security-2021-12-10/
- BitRise : https://blog.bitrise.io/post/bitrises-response-to-log4j-vulnerability-cve-2021-44228
- BitWarden : https://community.bitwarden.com/t/log4j-log4shell-cve-is-bitwarden-affected-due-to-docker-image/36177/2
- BioJava Java library for processing biological data, 6.0.3 : https://github.com/biojava/biojava/releases/tag/biojava-6.0.3
- BlackBerry :
   - BlackBerry Enterprise Mobility Server, 2.12 and above  : https://support.blackberry.com/community/s/article/90708
   - BlackBerry Workspaces On-prem Server, All : https://support.blackberry.com/community/s/article/90708
   - BlackBerry 2FA, All: https://support.blackberry.com/community/s/article/90708
- BlackBaud : https://kb.blackbaud.com/knowledgebase/articles/Article/198103
- Black Kite : https://blackkite.com/log4j-rce-vulnerability-log4shell-puts-millions-at-risk/
- Blancco : https://support.blancco.com/display/NEWS/2021/12/12/CVE-2021-44228+-+Critical+vulnerability+in+Apache+Log4j+library
- Blumira : https://www.blumira.com/cve-2021-44228-log4shell/
- BMC Software : 
   - https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - 3270 SUPEROPTIMIZER/CICS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Application Restart Control for Db2 : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Application Restart Control for IMS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Application Restart Control for VSAM : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Bladelogic Database Automation : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Batch Optimizer : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Capacity Management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Command Center for Security : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Console management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Cost Management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Datastream for Ops : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Defender for Db2 : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Defender for Ops Insight : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Defender for z/Linux : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Defender for z/OS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Defender for z/VM : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Defender TCP/IP Receiver : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Enterprise Connector : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Automation for Capping : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Common Rest API (CRA) : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops for Networks : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Infrastructure (MVI) - CRA component : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Insight : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Monitor for CMF : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Monitor for IMS Offline : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Monitor for IMS Online : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Monitor for USS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Monitor for z/OS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops Monitor SYSPROG Services : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Ops UI  : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Recovery for VSAM : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Security Administrator : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Security Policy Manager : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Security Privileged Access Manager (also called BMC AMI Security Breakglass) : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Security Self Service Password Reset : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Storage : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC AMI Utilities : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Client Management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Abend-Aid : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Application Audit : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware DevEnterprise : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Enterprise Common Components (ECC) : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Enterprise Services (CES) : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Enterprise Services : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware File-AID Data Privacy : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware File-AID Data Solutions : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware File-AID for DB2 : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware File-AID for IMS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware File-AID/MVS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware File-AID/RDX : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Hiperstation ALL Product Offerings : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware ISPW : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware iStrobe : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Program Analyzer : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Storage Backup and Recovery : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Storage Migration : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Storage Performance : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware ThruPut Manager : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Topaz Enterprise Data : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Topaz for Java Performance : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Topaz for Total Test : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Topaz Program Analysis : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Topaz Workbench : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Xpediter/CICS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Xpediter/Code Coverage : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Xpediter/TSO and IMS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware Xpediter/Xchange : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Compuware zAdviser : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Db2 Admin : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Db2 SQL Performance : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender Agent Configuration Manager : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender Agent for SAP : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender Agent for Unix/Linux : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender Agent for Windows : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender App for Splunk : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender SIEM Correlation Server : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender SIEM for Motorola : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender SIEM for NNT : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender SyslogDefender : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Defender Windows Agent for Splunk : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Discovery : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Helix Continuous Optimization – Agents : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Helix Continuous Optimization : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Helix Knowledge Management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC License Usage Collection Utility : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Plus Utilities : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - BMC Recovery Management – BMC AMI LogMaster, Recovery Manager, Copy, Recover : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Cloud Lifecycle Management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - CMDB : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Common Components: Next Generation Logger (NGL), Runtime Component System (RTCS), User Interface Middleware (UIM) : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Control-M : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - ExceptionReporter : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Footprints : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Helix Data Manager : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - KMs - Sybase KM & Linux (RHEV, Fix, Fix available in BMC’s Electronic Product Download site (EPD)  ,[source](https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - MainView Explorer : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - MainView Middleware Administrator : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - MainView Middleware Monitor : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - MainView Transaction Analyzer : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - PATROL Agent : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Release Process Management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Remedy ITSM (IT Service Management) : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Remedy Smart Reporting : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Resident Security Server : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - Track-It! : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight App Visibility Manager : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Automation Console : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Automation for Networks : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Automation for Servers - Data Warehouse : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Automation for Servers : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Capacity Optimization – Agents : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Capacity Optimization : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Infrastructure Management : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight IT Data Analytics : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Operations Management  : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Orchestration : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TrueSight Smart Reporting : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TSCO For Mainframes : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - TSOM Smart Reporting : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - ULTRAOPT/CICS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - ULTRAOPT/IMS : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
   - zDetect : https://community.bmc.com/s/news/aA33n000000TSUdCAO/bmc-security-advisory-for-cve202144228-log4shell-vulnerability
- Boomi DELL : https://community.boomi.com/s/article/Log4j-Vulnerability
- Boston Scientific : 
   - https://www.bostonscientific.com/content/dam/bostonscientific/corporate/product-security/bsc_statement_on_apache_log4j-v2.pdf
   - https://www.bostonscientific.com/content/dam/bostonscientific/corporate/product-security/apache_log4j_latitude_security_bulletin-v1.pdf
- Box : https://blog.box.com/boxs-statement-recent-log4j-vulnerability-cve-2021-44228
- Brainworks : https://www.brainworks.de/log4j-exploit-kerio-connect-workaround/
- BrightSign : https://brightsign.atlassian.net/wiki/spaces/DOC/pages/370679198/Security+Statement+Log4J+Meltdown+and+Spectre+Vulnerabilities#SecurityStatement%3ALog4J%2CMeltdownandSpectreVulnerabilities-JavaApacheLog4j
- Broadcom : https://support.broadcom.com/security-advisory/content/security-advisories/Symantec-Security-Advisory-for-Log4j-2-CVE-2021-44228-Vulnerability/SYMSA19793
- Broadcom Automic Automation : https://knowledge.broadcom.com/external/article?articleId=230308
- BuildSoft : https://bsoft.zendesk.com/hc/en-us/articles/4411821391631-Security-Advisory-Apache-Log4j-CVE-2021-44228

C
##############################################################################

- C4b XPHONE : https://www.c4b.com/de/news/log4j.php
- Caddy : https://caddy.community/t/apache-log4j-remote-code-execution-vulnerability/14403
- Calyptix Security : https://twitter.com/calyptix/status/1470498981147029507
- Camunda : https://forum.camunda.org/t/apache-log4j2-remote-code-execution-rce-vulnerability-cve-2021-44228/31910
- Canon : https://www.canon.com.au/support/support-news/support-news/security-advisory-potential-apache-log4j-vulnerability
- CarbonBlack : https://www.vmware.com/security/advisories/VMSA-2021-0028.html
- Carestream : https://www.carestream.com/en/us/services-and-support/cybersecurity-and-privacy
- CAS genesisWorld : https://helpdesk.cas.de/CASHelpdesk/FAQDetails.aspx?gguid=0x79F9E881EE3C46C1A71BE9EB3E480446
- Cato Networks : https://www.catonetworks.com/blog/cato-networks-rapid-response-to-the-apache-log4j-remote-code-execution-vulnerability/
- Celiveo : https://support.celiveo.com/support/solutions/articles/79000129570-cve-2021-44228-log4shell-log4j-vulnerability-celiveo-is-not-affected-
- Celonis : https://www.celopeers.com/s/article/Celonis-Advisory-CVE-2021-44228
- Celoxis : https://support.celoxis.com/hc/en-us/articles/4411989168537-Log4j-vulnerability-fixed-Updated-
- Cepheid : https://www.cepheid.com/en_US/legal/product-security-updates
- Cerberus FTP : https://support.cerberusftp.com/hc/en-us/articles/4412448183571-Cerberus-is-not-affected-by-CVE-2021-44228-log4j-0-day-vulnerability 
- Cerebrate : https://twitter.com/cerebrateproje1/status/1470347775141421058
- Cerebro : https://github.com/lmenezes/cerebro/blob/main/conf/logback.xml#L5
- CGM CompuGroup Medical SE & Co. KGaA Germany - Software Z1 : https://www.cgm.com/deu_de/plattformen/telematikinfrastruktur/service-und-updates/allgemeines-zu-updates/Information-zur-BSI-Warnmeldung.html
- ChaserSystems : https://chasersystems.com/discrimiNAT/blog/log4shell-and-its-traces-in-a-network-egress-filter/#are-chasers-products-affected
- Checkmarx plugin : https://github.com/jenkinsci/checkmarx-plugin/pull/83
- CheckMK : https://forum.checkmk.com/t/checkmk-not-affected-by-log4shell/28643/3
- CheckPoint : https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk176865
- Chef :
   - Chef, Infra Server, All : https://www.chef.io/blog/is-chef-vulnerable-to-cve-2021-44228-(log4j)
   - Chef, Automate, All : https://www.chef.io/blog/is-chef-vulnerable-to-cve-2021-44228-(log4j)
   - Chef, Backend, All : https://www.chef.io/blog/is-chef-vulnerable-to-cve-2021-44228-(log4j)
- Ciphermail : https://www.ciphermail.com/blog/ciphermail-gateway-and-webmail-messenger-are-not-vulnerable-to-cve-2021-44228.html
- CIS : https://cisecurity.atlassian.net/servicedesk/customer/portal/15/article/2434301961
- Cisco: https://tools.cisco.com/security/center/content/CiscoSecurityAdvisory/cisco-sa-apache-log4j-qRuKNEbd
- Citrix : https://support.citrix.com/article/CTX335705
- Claris : https://community.claris.com/en/s/article/Q-A-Claris-products-and-the-Apache-Log4j-vulnerability
- Clavister :
   - Clavister - NetWall,  : https://www.clavister.com/advisories/security/clav-sa-0297-high-severity-vulnerability-in-apache-log4j-2
   - Clavister - NetShield : https://www.clavister.com/advisories/security/clav-sa-0297-high-severity-vulnerability-in-apache-log4j-2
   - Clavister - InControl : https://www.clavister.com/advisories/security/clav-sa-0297-high-severity-vulnerability-in-apache-log4j-2
   - Clavister - OneConnect : https://www.clavister.com/advisories/security/clav-sa-0297-high-severity-vulnerability-in-apache-log4j-2
   - Clavister - EasyAccess , <= 4.1.2,  : https://kb.clavister.com/343410234/high-severity-vulnerability-in-apache-log4j-2
   - Clavister - InCenter , <= 1.68.03, 2.0.0 and 2.1.0 : https://kb.clavister.com/343410462/vulnerability-in-apache-log4j-2-which-is-used-in-incenter
- Cloudera : https://my.cloudera.com/knowledge/TSB-2021-545-Critical-vulnerability-in-log4j2-CVE-2021-44228?id=332019
- CloudFlare : https://blog.cloudflare.com/cve-2021-44228-log4j-rce-0-day-mitigation/
- Cloudian HyperStore : https://cloudian-support.force.com/s/article/SECURITY-Cloudian-HyperStore-Log4j-vulnerability-CVE-2021-44228
- Cloudogu : https://community.cloudogu.com/t/security-vulnerability-log4shell-cve-2021-44228/417
- Cloudron : https://forum.cloudron.io/topic/6153/log4j-and-log4j2-library-vulnerability?lang=en-US
- CMND.io : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-4002005
- Clover : https://community.clover.com/articles/35868/apache-log4j-vulnerability-cve-2021-44228.html
- CPanel : https://forums.cpanel.net/threads/log4j-cve-2021-44228-does-it-affect-cpanel.696249/
- Code42 : https://support.code42.com/Terms_and_conditions/Code42_customer_support_resources/Code42_response_to_industry_security_incidents
- CodeBeamer : https://codebeamer.com/cb/wiki/19872365
- Codesys : https://www.codesys.com/news-events/news/article/log4j-not-used-in-codesys.html
- CodeTwo : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995424
- Cohesity : https://support.cohesity.com/s/article/Security-Advisory-Apache-Log4j-Remote-Code-Execution-RCE-CVE-2021-44228
- CommVault : https://documentation.commvault.com/v11/essential/146231_security_vulnerability_and_reporting.html
- Compumatica : 
   - Compumatica, CryptoGuard, all : https://www.compumatica.com/nieuws-bericht/important-information-log4j-vulnerability-compumatica-secure-networks/
   - Compumatica, CompuMail Gateway, all : https://www.compumatica.com/nieuws-bericht/important-information-log4j-vulnerability-compumatica-secure-networks/
   - Compumatica, Compuwall, all : https://www.compumatica.com/nieuws-bericht/important-information-log4j-vulnerability-compumatica-secure-networks/
   - Compumatica, MagiCtwin, all : https://www.compumatica.com/nieuws-bericht/important-information-log4j-vulnerability-compumatica-secure-networks/
   - Compumatica, MASC, all : https://www.compumatica.com/nieuws-bericht/important-information-log4j-vulnerability-compumatica-secure-networks/
- Comrex : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3996774
- ConcreteCMS.com : https://www.concretecms.com/about/blog/security/concrete-log4j-zero-day-exploit
- Confluent : https://support.confluent.io/hc/en-us/articles/4412615410580-CVE-2021-44228-log4j2-vulnerability
- Connect2id : https://connect2id.com/blog/connect2id-server-12-5-1
- ConnectWise : https://www.connectwise.com/company/trust/advisories 
- ContrastSecurity : https://support.contrastsecurity.com/hc/en-us/articles/4412612486548
- ControlUp : https://status.controlup.com/incidents/qqyvh7b1dz8k
- COPADATA : https://www.copadata.com/en/support-services/knowledge-base-faq/pare-products-in-the-zenon-product-family-affect-4921/
- Coralogix : https://twitter.com/Coralogix/status/1469713430659559425
- CouchBase : https://forums.couchbase.com/t/ann-elasticsearch-connector-4-3-3-4-2-13-fixes-log4j-vulnerability/32402
- Cradlepoint : https://cradlepoint.com/vulnerability-alerts/cve-2021-44228-apache-log4j-security-vulnerabilities/
- Crate CrateDB : https://github.com/crate/crate/pull/11968#issuecomment-994577174
- Crestron : https://www.crestron.com/Security/Security_Advisories/Apache-Log4j
- CrushFTP : https://www.crushftp.com/download.html
- CryptShare : https://www.cryptshare.com/en/support/cryptshare-support/#c67572
- Cumul.io https://status.cumul.io/#incidents
- CURL libcurl : https://twitter.com/bagder/status/1470879113116360706
- CyberArk : https://cyberark-customers.force.com/s/article/Critical-Vulnerability-CVE-2021-44228
- Cybereason : https://www.cybereason.com/blog/cybereason-solutions-are-not-impacted-by-apache-log4j-vulnerability-cve-2021-44228
- CyberRes : https://community.microfocus.com/cyberres/b/sws-22/posts/summary-of-cyberres-impact-from-log4j-or-logshell-logjam-cve-2021-44228
- Cydar Medical : https://www.cydarmedical.com/news/cydar-response-to-apache-log4j-vulnerability
- Cytoscape : https://cytoscape.org/common_issues.html#log4shell

D
##############################################################################

- DarkTrace : https://customerportal.darktrace.com/inside-the-soc/get-article/201
- Dashlane : https://twitter.com/DashlaneSupport/status/1470933847932030976
- Dassault Systèmes : https://kb.dsxclient.3ds.com/mashup-ui/page/resultqa?id=QA00000102301e
- Databricks : https://docs.google.com/document/d/e/2PACX-1vREjwZk17BAHGwj5Phizi4DPFS9EIUbAMX-CswlgbFwqwKXNKZC8MrT-L6wUgfIChsSHtvd_QD3-659/pub
- DataDog : https://www.datadoghq.com/log4j-vulnerability/
- Dataminer : https://community.dataminer.services/responding-to-log4shell-vulnerability/
- Datev : https://www.datev-community.de/t5/Freie-Themen/Log4-J-Schwachstelle/m-p/258185/highlight/true#M14308
- Datto : https://www.datto.com/blog/dattos-response-to-log4shell
- dCache.org : https://www.dcache.org/post/log4j-vulnerability/
- DCM4CHE.org : https://github.com/dcm4che/dcm4che/issues/1050
- Debian : https://security-tracker.debian.org/tracker/CVE-2021-44228
- Deepinstinct : https://www.deepinstinct.com/blog/log4shell-cve-2021-44228-what-you-need-to-know
- Dell : https://www.dell.com/support/kbdoc/en-us/000194372/dsn-2021-007-dell-response-to-apache-log4j-remote-code-execution-vulnerability
- DELL : https://www.dell.com/support/kbdoc/en-us/000194416/additional-information-for-apache-log4j-remote-code-execution-vulnerability-cve-2021-44228
- Denequa : https://denequa.de/log4j-information.html
- Device42 : https://blog.device42.com/2021/12/13/log4j-zero-day/
- Devolutions : https://blog.devolutions.net/2021/12/critical-vulnerability-in-log4j/
- Diebold Nixdorf : https://www.dieboldnixdorf.com/en-us/apache
- Digicert : https://knowledge.digicert.com/alerts/digicert-log4j-response.html
- Digilent waveforms : https://forum.digilentinc.com/topic/22531-has-waveforms-been-affected-by-the-log4j-vulnerability-cve-2021-44228/
- Digital AI : https://support.digital.ai/hc/en-us/articles/4412377686674-Log4J-Vulnerability-to-Zero-Day-Exploit-and-Digital-ai#overview-0-1
- DNSFilter : https://www.dnsfilter.com/blog/dnsfilter-response-to-log4j-vulnerability
- Docker : https://www.docker.com/blog/apache-log4j-2-cve-2021-44228/
- Docusign : https://www.docusign.com/trust/alerts/alert-docusign-statement-on-the-log4j2-vulnerability
- DRAW.IO : https://twitter.com/drawio/status/1470061320066277382
- DrayTek : https://www.draytek.co.uk/support/security-advisories/kb-advisory-dec2021-log4j
- DropWizard : https://twitter.com/dropwizardio/status/1469285337524580359
- DSpace :https://groups.google.com/g/dspace-community/c/Fa4VdjiiNyE
- DynaTrace : https://community.dynatrace.com/t5/Dynatrace-Open-Q-A/Impact-of-log4j-zero-day-vulnerability/m-p/177259/highlight/true#M19282

E
##############################################################################

- EasyRedmine : https://www.easyredmine.com/news/easy-redmine-application-is-not-affected-by-the-vulnerability-cve-2021-44228
- EclecticIQ : https://docs.eclecticiq.com/security-advisories/security-issues-and-mitigation-actions/eiq-2021-0016-2
- Eclipse Foundation : https://wiki.eclipse.org/Eclipse_and_log4j2_vulnerability_(CVE-2021-44228)
- EFI : https://communities.efi.com/s/article/Are-Fiery-Servers-vulnerable-to-CVE-2021-44228-Apache-Log4j2?language=en_US
- EGroupware : https://help.egroupware.org/t/uk-de-statement-log4j-log4shell/76430
- EHRBase : https://github.com/ehrbase/ehrbase/issues/700
- Elastic : https://discuss.elastic.co/t/apache-log4j2-remote-code-execution-rce-vulnerability-cve-2021-44228-esa-2021-31/291476
- Elekta : https://www.elekta.com/software-solutions/product-security
- Ellucian (Banner and Colleague Higher Education SIS) : https://www.ellucian.com/news/ellucian-response-apache-log4j-issue
- Emerson : https://www.emerson.com/documents/automation/emerson-cyber-security-notification-en-7881618.pdf
- EnterpriseDT : https://enterprisedt.com/blogs/announcements/enterprisedt-does-not-use-log4j/
- EPICOR : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3996932
- ERICOM : https://blog.ericom.com/ericom-software-products-and-the-log4shell-exploit-cve-2021-44228/
- ESA SNAP Toolbox : https://forum.step.esa.int/t/snap-not-affected-by-log4j-vulnerability/34167
- ESET : https://support.eset.com/en/alert8188-information-regarding-the-log4j2-vulnerability
- ESI Group : https://myesi.esi-group.com/support/apache-log4j-vulnerability.
- ESRI : https://www.esri.com/arcgis-blog/products/arcgis-enterprise/administration/arcgis-software-and-cve-2021-44228-aka-log4shell-aka-logjam/
- Estos : https://support.estos.de/de/sicherheitshinweise/estos-von-kritischer-schwachstelle-in-log4j-cve-2021-44228-nicht-betroffen
- Eurolinux : https://en.euro-linux.com/blog/critical-vulnerability-cve-2021-44228-in-apache-log4j/
- EVLLABS JGAAP : https://github.com/evllabs/JGAAP/releases/tag/v8.0.2
- Evolveum Midpoint : https://evolveum.com/midpoint-not-vulnerable-to-log4shell/
- Ewon : https://hmsnetworks.blob.core.windows.net/www/docs/librariesprovider10/downloads-monitored/manuals/release-notes/ecatcher_releasenotes.txt?sfvrsn=4f054ad7_42
- Exabeam : https://community.exabeam.com/s/discussions?t=1639379479381
- Exact : https://www.exact.com/news/general-statement-apache-leak
- Exivity : https://docs.exivity.com/getting-started/releases/announcements#announcement-regarding-cve-2021-44228
- ExtraHop : https://forums.extrahop.com/t/extrahop-update-on-log4shell/8148
- eXtreme Hosting : https://extremehosting.nl/log4shell-log4j/
- Extreme Networks : https://extremeportal.force.com/ExtrArticleDetail?an=000100806
- Extron : https://www.extron.com/featured/Security-at-Extron/extron-security

F
##############################################################################

- F5 Networks : https://support.f5.com/csp/article/K19026212
- F-Secure https://status.f-secure.com/incidents/sk8vmr0h34pd
- Fastly : https://www.fastly.com/blog/digging-deeper-into-log4shell-0day-rce-exploit-found-in-log4j
- FB-PRO GmbH : https://www.fb-pro.com/log4shell-log4j-luecke-enforce-administrator/
- FAST LTA : https://blog.fast-lta.de/en/log4j2-vulnerability
- FedEx : https://www.fedex.com/en-us/service-alerts.html#weatherassess
- FileCatalyst : https://support.filecatalyst.com/index.php/Knowledgebase/Article/View/advisory-log4j-zero-day-security-vulnerability
- FileCap : https://mailchi.mp/3f82266e0717/filecap-update-version-511
- FileCloud : https://www.getfilecloud.com/supportdocs/display/cloud/Advisory+2021-12-2+Impact+of+Apache+Log4j2+Vulnerability+on+FileCloud+Customers
- FileWave : https://kb.filewave.com/display/KB/Security+Notice:+Apache+log4j+Vulnerability+CVE-2021-44228
- FINVI : https://finvi.com/support/
- FireDaemon : https://kb.firedaemon.com/support/solutions/articles/4000178630
- Fisher & Paykel Healthcare : https://www.fphcare.com/us/our-company/contact-us/product-security/
- Flexagon : https://flexagon.com/what-is-the-impact-of-log4j-vulnerability-cve-2021-44228-on-flexdeploy/
- Flexera : https://community.flexera.com/t5/Community-Notices/Flexera-s-response-to-Apache-Log4j-2-remote-code-execution/ba-p/216934
- FlyWheel : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995147
- ForcePoint : https://support.forcepoint.com/s/article/CVE-2021-44228-Java-log4j-vulnerability-mitigation-with-Forcepoint-Security-Manager
- Forescout : https://forescout.force.com/support/s/article/Important-security-information-related-to-Apache-Log4j-utility-CVE-2021-44228
- ForgeRock : https://backstage.forgerock.com/knowledge/kb/article/a39102625
- Fortinet : https://www.fortiguard.com/psirt/FG-IR-21-245
- FTAPI : https://www.ftapi.com/blog/kritische-sicherheitslucke-in-log4j-ftapi-reagiert/#
- Fujitsu : https://support.ts.fujitsu.com/ProductSecurity/content/Fujitsu-PSIRT-PSS-IS-2021-121000-Security-Notice-SF.pdf
- FusionAuth : https://fusionauth.io/blog/2021/12/10/log4j-fusionauth/

G
##############################################################################

- Gearset : https://docs.gearset.com/en/articles/5806813-gearset-log4j-statement-dec-2021
- GE Healthcare : https://www.gehealthcare.com/security
- Genesys : https://www.genesys.com/blog/post/genesys-update-on-the-apache-log4j-vulnerability
- GeoServer : http://geoserver.org/announcements/2021/12/13/logj4-rce-statement.html
- Gerrit code review : https://www.gerritcodereview.com/2021-12-13-log4j-statement.html
- GFI : https://techtalk.gfi.com/impact-of-log4j-vulnerability-on-gfi/
- Ghidra : https://github.com/NationalSecurityAgency/ghidra/blob/2c73c72f0ba2720c6627be4005a721a5ebd64b46/README.md#warning
- Gigamon : https://community.gigamon.com/gigamoncp/s/article/Are-Gigamon-products-affected-by-CVE-2021-44228
- GitHub : https://github.com/advisories/GHSA-jfh8-c2jp-5v3q
- GitHub Response : https://github.blog/2021-12-13-githubs-response-to-log4j-vulnerability-cve-2021-44228/
- GitLab : https://forum.gitlab.com/t/cve-2021-4428/62763
- Globus : https://groups.google.com/a/globus.org/g/discuss/c/FJK0q0NoUC4
- GLPI Project : https://forum.glpi-project.org/viewtopic.php?pid=488631#p488631
- GoAnywhere : https://www.goanywhere.com/cve-2021-44228-goanywhere-mitigation-steps
- GoCD : https://www.gocd.org/2021/12/14/log4j-vulnerability.html
- Google Cloud Global Products coverage : https://cloud.google.com/log4j2-security-advisory
- Google Cloud Armor WAF : https://cloud.google.com/blog/products/identity-security/cloud-armor-waf-rule-to-help-address-apache-log4j-vulnerability
- Gradle : https://blog.gradle.org/log4j-vulnerability
- Grafana : https://grafana.com/blog/2021/12/14/grafana-labs-core-products-not-impacted-by-log4j-cve-2021-44228-and-related-vulnerabilities/
- Grandstream : https://blog.grandstream.com/press-releases/grandstream-products-unaffected-by-log4j-vulnerability?hsLang=en
- GratWiFi WARNING I can't confirm it: https://www.facebook.com/GratWiFi/posts/396447615600785
- Gravitee.io : https://www.gravitee.io/news/about-the-log4j-cvss-10-critical-vulnerability
- Gravwell : https://www.gravwell.io/blog/cve-2021-44228-log4j-does-not-impact-gravwell-products
- GrayLog : https://www.graylog.org/post/graylog-update-for-log4j
- GreenShot : https://greenshot.atlassian.net/browse/BUG-2871
- GuardedBox : https://twitter.com/GuardedBox/status/1469739834117799939
- Guidewire : https://community.guidewire.com/s/article/Update-to-customers-who-have-questions-about-the-use-of-log4j-in-Guidewire-products

H
##############################################################################

- HackerOne : https://twitter.com/jobertabma/status/1469490881854013444
- HAProxy : https://www.haproxy.com/blog/december-2021-log4shell-mitigation/
- HarmanPro AMX : https://help.harmanpro.com/apache-log4j-vulnerability
- Hashicorp : https://discuss.hashicorp.com/t/hcsec-2021-32-hashicorp-response-to-apache-log4j-2-security-issue-cve-2021-44228/33138
- Hazelcast : https://support.hazelcast.com/s/article/Security-Advisory-for-Log4Shell
- HCL Global : https://support.hcltechsw.com/csm/en?id=kb_article&sysparm_article=KB0095490
- HelpSystems Clearswift : https://community.helpsystems.com/kb-nav/kb-article/?id=37becc1c-255c-ec11-8f8f-6045bd006687
- Hewlett Packard Enterprise HPE : https://support.hpe.com/hpesc/public/docDisplay?docLocale=en_US&docId=hpesbgn04215en_us
- Hewlett Packard Enterprise HPE GLOBAL : https://techhub.hpe.com/eginfolib/securityalerts/Apache%20Software%20Log4j/Apache_Software_Log4j.html
- Hexagon : https://supportsi.hexagon.com/help/s/article/Security-Vulnerability-CVE-2021-44228-log4j-2?language=en_US
- Hikvision : https://video.xortec.de/media/pdf/87/e8/03/kw50_Update-for-Apache-Log4j2-Issue-Hikvision_official.pdf
- Hitachi Vantara : https://support.pentaho.com/hc/en-us/articles/4416229254541-log4j-2-zero-day-vulnerability-No-impact-to-supported-versions-of-Pentaho-
- Hologic software : https://www.hologic.com/support/usa/breast-skeletal-products-cybersecurity
- HomeAssistant : https://community.home-assistant.io/t/looking-for-advice-on-log4j/367250/6
- Honeywell : https://www.honeywell.com/us/en/press/2021/12/honeywells-statement-on-java-apache-log4j-logging-framework-vulnerability
- HostiFi : https://twitter.com/hostifi_net/status/1469511114824339464
- Huawei : https://www.huawei.com/en/psirt/security-notices/huawei-sn-20211210-01-log4j2-en
- Hubspot : https://community.hubspot.com/t5/APIs-Integrations/Log4J-day-zero-exploit-CVE-2021-44228/td-p/541949
- Hyte.IO : https://hyte.io/cve-2021-44228/

I
##############################################################################

- I2P : https://geti2p.net/en/blog/post/2021/12/11/i2p-unaffected-cve-2021-44228
- I-Net software : https://faq.inetsoftware.de/t/statement-about-cve-2021-44228-log4j-vulnerability-concerning-i-net-software-products/269/3
- IBA-AG : https://www.iba-ag.com/en/security
- Ibexa : https://developers.ibexa.co/security-advisories/cve-2021-44228-log4j-vulnerability
- IBM : 
   - https://www.ibm.com/support/pages/node/6525548
   - IBM PSIRT Blog https://www.ibm.com/blogs/psirt/?s=log4j 
   - IBM Tivoli Storage Manager (formerly Spectrum Protect : https://www.ibm.com/support/pages/ibm-spectrum-protect-downloads-latest-fix-packs-and-interim-fixes
- IDS : https://en.ids-imaging.com/news-article/information-log4j-vulnerability.html
- IFS : https://community.ifs.com/announcements-278/urgent-bulletin-ifs-advisory-ifs-products-services-and-log4j-cve-2021-44228-16436
- IGEL : https://kb.igel.com/securitysafety/en/isn-2021-11-ums-log4j-vulnerability-54086712.html
- Ignite Realtime OpenFire : https://discourse.igniterealtime.org/t/openfire-4-6-5-released/91108
- iGrafx : https://www.igrafx.com/igrafx-thwarts-log4j-vulnerability/
- Illumina : https://support.illumina.com/bulletins/2021/121/investigation-of-log4j-vulnerability-with-clarity-lims.html
- Illuminated Cloud : https://illuminatedcloud.blogspot.com/2021/12/illuminated-cloud-2-and-log4j-security.html
- Illumio : https://support.illumio.com/knowledge-base/articles/Customer-Security-Advisory-on-log4j-RCE-CVE-2021-44228.html
- IManage : https://help.imanage.com/hc/en-us/articles/4412696236699-ADVISORY-Security-vulnerability-CVE-2021-44228-in-third-party-component-Apache-Log4j2#h_3164fa6c-4717-4aa1-b2dc-d14d4112595e 
- Impero Software : https://www.imperosoftware.com/us/impero-software-and-the-log4j-vulnerability/
- Imperva : https://docs.imperva.com/howto/9111b8a5/
- Inductive Automation : https://support.inductiveautomation.com/hc/en-us/articles/4416204541709-Regarding-CVE-2021-44228-Log4j-RCE-0-day
- IndustrialDefender : https://www.industrialdefender.com/cve-2021-44228-log4j/
- infinidat : https://support.infinidat.com/hc/en-us/articles/4413483145489-INFINIDAT-Support-Announcement-2021-010-Log4Shell-CVE-2021-44228
- InfluxData : https://www.influxdata.com/blog/apache-log4j-vulnerability-cve-2021-44228/
- Infoblox : https://support.infoblox.com/articles/Knowledge/Infoblox-NIOS-and-BloxOne-products-not-vulnerable-to-CVE-2021-44228
- Informatica : https://network.informatica.com/community/informatica-network/blog/2021/12/10/log4j-vulnerability-update
- Inovonics Broadcast : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3996774
- Instana : https://status.instana.io/incidents/4zgcd2gzf4jw
- Instructure : https://community.canvaslms.com/t5/Community-Users/Instructure-amp-the-Apache-Log4j2-Vulnerability/ba-p/501907
- Integrative Genomics Viewer IGV : https://github.com/igvteam/igv/commit/40aa5e0c6b5f2eac0a1528658189fd7de8f20347
- Intel : https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00646.html
- InterSystems : https://www.intersystems.com/gt/apache-log4j2/
- Interworks : https://interworks.com/blog/2021/12/14/log4j-vulnerability-cve-2021-44228-information/
- Intuiface : https://twitter.com/Intuiface/status/1471095195664015363
- Intuit Quickbooks : https://quickbooks.intuit.com/learn-support/en-us/quickbooks-time/0-day-log4j-exploit/00/990291
- iRedMail : https://forum.iredmail.org/topic18605-log4j-cve202144228.html
- IronCore Labs : https://twitter.com/IronCoreLabs/status/1469359583147659269
- Ironnet : https://www.ironnet.com/blog/ironnet-security-notifications-related-to-log4j-vulnerability
- ISLONLINE : https://blog.islonline.com/2021/12/13/isl-online-is-not-affected-by-log4shell-vulnerability/
- ISPNext : https://github.com/NCSC-NL/log4shell/blob/main/software/vendor-statements/ISPNext.png
- Ivanti : https://forums.ivanti.com/s/article/CVE-2021-44228-Java-logging-library-log4j-Ivanti-Products-Impact-Mapping?language=en_US
- IVPN : https://www.reddit.com/r/IVPN/comments/rgw3nd/comment/homt79k/?utm_source=share&utm_medium=web2x&context=3

J
##############################################################################

- Jamasoftware : https://community.jamasoftware.com/communities/community-home/digestviewer/viewthread?MessageKey=06d26f9c-2abe-4c10-93d4-c0f6c8a01b22&CommunityKey=c9d20d4c-5bb6-4f19-92eb-e7cee0942d51&tab=digestviewer#bm06d26f9c-2abe-4c10-93d4-c0f6c8a01b22
- Jam Software : https://knowledgebase.jam-software.de/7577
- JAMF : https://docs.jamf.com/technical-articles/Mitigating_the_Apache_Log4j_2_Vulnerability.html
- Jaspersoft : https://community.jaspersoft.com/wiki/apache-log4j-vulnerability-update-jaspersoft-products
- JazzSM DASH IBM : https://www.ibm.com/support/pages/node/6525552
- Jedox : https://www.jedox.com/en/trust/
- Jenkins : https://www.jenkins.io/blog/2021/12/10/log4j2-rce-CVE-2021-44228/ 
- JetBrains Global :https://blog.jetbrains.com/blog/2021/12/13/log4j-vulnerability-and-jetbrains-products-and-services/
- JetBrains Teamcity : https://youtrack.jetbrains.com/issue/TW-74298
- JetBrains YouTrack and Hub : https://youtrack.jetbrains.com/issue/JT-67582
- JFROG : https://jfrog.com/knowledge-base/general-jfrog-services-are-not-affected-by-vulnerability-cve-2021-44228/
- Jitterbit : https://success.jitterbit.com/display/DOC/Mitigating+the+Apache+Log4j2+JNDI+Vulnerability
- Jitsi : https://github.com/jitsi/security-advisories/blob/4e1ab58585a8a0593efccce77d5d0e22c5338605/advisories/JSA-2021-0004.md
- JPOS : https://github.com/jpos/jPOS/commit/d615199a1bdd35c35d63c07c10fd0bdbbc96f625
- JobRouter : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995497
- Johnson Controls : https://www.johnsoncontrols.com/-/media/jci/cyber-solutions/product-security-advisories/2021/jci-psa-2021-23-v3.pdf?la=en&hash=1FC58B65D948E106055CA63184EFBCAB5C7DD9A1
- Journyx : https://community.journyx.com/support/solutions/articles/9000209044-apache-log4j-2-vulnerability-cve-2021-44228-
- Jump Desktop : https://support.jumpdesktop.com/hc/en-us/articles/4416720395021-Log4j-CVE-2021-44228-CVE-2021-45046-Statement
- Juniper Networks : https://kb.juniper.net/InfoCenter/index?page=content&id=JSA11259
- Justice Systems : https://www.justicesystems.com/services/support/

K
##############################################################################

- K15t : https://help.k15t.com/k15t-apps-and-log4shell-193401141.html
- K6 : https://k6.io/blog/k6-products-not-impacted-by-cve-2021-44228/
- Kafka Connect CosmosDB : https://github.com/microsoft/kafka-connect-cosmosdb/blob/0f5d0c9dbf2812400bb480d1ff0672dfa6bb56f0/CHANGELOG.md
- Karakun : https://board.karakun.com/viewtopic.php?f=21&t=8351
- Kaseya : https://helpdesk.kaseya.com/hc/en-gb/articles/4413449967377-Log4j2-Vulnerability-Assessment
- Katalon : https://forum.katalon.com/t/katalon-response-to-the-log4j2-exploit-cve-2021-44228/60742/5
- Keeper Security : https://www.keepersecurity.com/blog/2021/12/15/public-notice-regarding-the-apache-foundation-log4j-vulnerability/
- KEMP : https://support.kemptechnologies.com/hc/en-us/articles/4416430695437-CVE-2021-44228-Log4j2-Exploit
- KEMP 2 : https://support.kemptechnologies.com/hc/en-us/articles/4416473820045-Progress-Kemp-LoadMaster-protects-from-security-vulnerability-Apache-Log4j-2-CVE-2021-44228-
- Keycloak : https://github.com/keycloak/keycloak/discussions/9078
- Keypass : https://sourceforge.net/p/keepass/discussion/329220/thread/4643c5ec4f/?limit=250#c0bc
- KIE : https://blog.kie.org/2021/12/kie-log4j2-exploit-cve-2021-44228.html
- KiteWorks : https://www.kiteworks.com/kiteworks-news/log4shell-apache-vulnerability-what-kiteworks-customers-need-to-know/
- Kofax : https://knowledge.kofax.com/MFD_Productivity/SafeCom/Product_Information/SafeCom_and_Log4j_vulnerability_(CVE-2021-44228)
- Komoot Photon : https://github.com/komoot/photon/issues/620
- Konica Minolta : 
   - https://kmbs.konicaminolta.us/kmbs/support-downloads/security-announcements/announcements/apache-log4j-vulnerability
   - https://www.konicaminolta.de/de-de/support/log4j
- Kronos UKG : https://community.kronos.com/s/feed/0D54M00004wJKHiSAO?language=en_US
- Kyberna : https://www.kyberna.com/detail/log4j-sicherheitsluecke
- Kyocera : https://www.kyoceradocumentsolutions.de/de/support/sicherheitsluecke-Log4j.html

L
##############################################################################

- L3Harris Geospatial : https://www.l3harrisgeospatial.com/Support/Self-Help-Tools/Help-Articles/Help-Articles-Detail/ArtMID/10220/ArticleID/24141/Impact-of-Log4j-Java-Security-Vulnerability-CVE-2021-44228-on-L3Harris-Geospatial-software
- L-Soft : http://www.lsoft.com/news/log4jinfo.asp
- LabCollector : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995088
- Lancom Systems : https://www.lancom-systems.com/service-support/instant-help/general-security-information/
- Lansweeper : https://www.lansweeper.com/vulnerability/critical-log4j-vulnerability-affects-millions-of-applications/
- Laserfiche : https://answers.laserfiche.com/questions/194037/Do-any-Laserfiche-products-use-the-Apache-log4j-library#194038
- LastPass : https://support.logmeininc.com/lastpass/help/log4j-vulnerability-faq-for-lastpass-universal-proxy
- LaunchDarkly : https://launchdarkly.com/blog/audit-shows-systems-unaffected-by-log4j/
- Leanix : https://www.leanix.net/en/blog/log4j-vulnerability-log4shell
- Leica Biosystems : https://www.leicabiosystems.com/about/product-security/
- Lenovo : https://support.lenovo.com/ro/en/product_security/len-76573
- LeoStream : https://support.leostream.com/support/discussions/topics/66000507567
- Lepide : https://www.lepide.com/news/lepide-statement-on-cve-2021-44228-the-apache-log4j-vulnerability/
- Let's Encrypt : https://community.letsencrypt.org/t/log4j-vulnerability-cve-2021-44228/167464
- LucentSKY : https://twitter.com/LucentSky/status/1469358706311974914
- LibreNMS : https://community.librenms.org/t/is-librenms-affected-by-vulnerable-to-cve-2021-25218-cve-2021-44228/17675/6
- LifeRay : https://liferay.dev/blogs/-/blogs/log4j2-zero-day-vulnerability
- LifeSize : https://community.lifesize.com/s/article/Apache-Log4j2-CVE-2021-44228
- Lightbend : https://discuss.lightbend.com/t/regarding-the-log4j2-vulnerability-cve-2021-44228/9275
- Lime CRM : https://docs.lime-crm.com/security/lcsec21-01
- LIONGARD : https://insights.liongard.com/faq-apache-log4j-vulnerability
- LiquidFiles : https://mailchi.mp/liquidfiles/liquidfiles-log4j?e=%5BUNIQID%5D
- LiveAction : https://documentation.liveaction.com/LiveNX/LiveNX%2021.5.1%20Release%20Notes/Release%20Notes%20LiveNX%2021.5.1.1.3
- Loftware : https://help.loftware.com/lps-kb/content/log4j%20cve-2021-44228.htm?Highlight=CVE-2021-44228
- LogiAnalytics : https://devnet.logianalytics.com/hc/en-us/articles/4415781801751-Statement-on-Log4j-Vulnerability-CVE-2021-44228-
- LogicMonitor : https://www.logicmonitor.com/support/log4shell-security-vulnerability-cve-2021-44228
- LogMeIn : https://community.logmein.com/t5/LogMeIn-Central-Discussions/LOG4J-Vulnerability/m-p/280317/highlight/true#M8327
- LogRhythm : https://community.logrhythm.com/t5/Product-Security/LogRhythm-Response-to-the-Apache-Log4J-Vulnerability-Log4Shell/td-p/494068
- Looker : https://docs.google.com/document/d/e/2PACX-1vQGN1AYNMHxsRQ9AZNu1bKyTGRUSK_9xkQBge-nu4p8PYvBKIYHhc3914KTfVtDFIXtDhc3k6SZnR2M/pub
- LucaNet : https://www.lucanet.com/en/blog/update-vulnerability-log4j
- Lucee : https://dev.lucee.org/t/lucee-is-not-affected-by-the-log4j-jndi-exploit-cve-2021-44228/9331/4

M
##############################################################################

- Macchina io : https://twitter.com/macchina_io/status/1469611606569099269
- MailCow : https://github.com/mailcow/mailcow-dockerized/issues/4375
- MailStore : https://www.mailstore.com/en/blog/mailstore-affected-by-log4shell/
- Maltego : https://www.maltego.com/blog/our-response-to-log4j-cve-2021-44228/
- ManageEngine Zoho  : https://pitstop.manageengine.com/portal/en/community/topic/log4j-ad-manager-plus
- ManageEngine Zoho  : https://pitstop.manageengine.com/portal/en/community/topic/update-on-the-recent-apache-log4j2-vulnerability-impact-on-manageengine-on-premises-products-1
- MariaDB : https://mariadb.com/resources/blog/log4shell-and-mariadb-cve-2021-44228/
- Marin software : https://insights.marinsoftware.com/marin-software/marin-software-statement-on-log4j-vulnerability/
- MathWorks Matlab : https://www.mathworks.com/matlabcentral/answers/1610640-apache-log4j-vulnerability-cve-2021-44228-how-does-it-affect-matlab-run-time
- Matillion : https://documentation.matillion.com/docs/security-advisory-14th-december-2021
- Matomo : https://forum.matomo.org/t/matomo-is-not-concerned-by-the-log4j-security-breach-cve-2021-44228-discovered-on-december-2021-the-9th/44089
- Mattermost FocalBoard : https://forum.mattermost.org/t/log4j-vulnerability-concern/12676
- McAfee : https://kc.mcafee.com/corporate/index?page=content&id=KB95091
- MediathekView.de : https://mediathekview.de/changelog/13-8-1/
- MediaWiki : https://www.mediawiki.org/wiki/Wikibase/Announcements/2021-12-14/en
- Medtronic : https://global.medtronic.com/xg-en/product-security/security-bulletins/log4j-vulnerabilities.html
- MEINBERG : https://www.meinbergglobal.com/english/news/meinberg-lantime-and-microsync-systems-not-at-risk-from-log4j-security-exploit.htm
- Memurai : https://www.memurai.com/blog/apache-log4j2-cve-2021-44228
- Mendix :https://status.mendix.com/incidents/8j5043my610c
- Metabase : https://github.com/metabase/metabase/commit/8bfce98beb25e48830ac2bfd57432301c5e3ab37
- MicroFocus : https://portal.microfocus.com/s/customportalsearch?language=en_US&searchtext=CVE-2021-44228
- Microsoft : 
   - https://www.microsoft.com/security/blog/2021/12/11/guidance-for-preventing-detecting-and-hunting-for-cve-2021-44228-log4j-2-exploitation/
   - https://msrc-blog.microsoft.com/2021/12/11/microsofts-response-to-cve-2021-44228-apache-log4j2/
- Microstrategy : https://community.microstrategy.com/s/article/MicroStrategy-s-response-to-CVE-2021-44228-The-Log4j-0-Day-Vulnerability?language=en_US
- Midori Global : https://www.midori-global.com/blog/2021/12/15/cve-2021-44228-log4shell-midori-apps-are-not-affected
- Mikrotik : https://forum.mikrotik.com/viewtopic.php?p=897938
- Milestone sys : https://supportcommunity.milestonesys.com/s/article/Log4J-vulnerability-faq?language=en_US
- Mimecast : https://community.mimecast.com/s/article/Mimecast-Information-for-Customers-on-the-Log4Shell-Vulnerability
- Minecraft : https://www.minecraft.net/en-us/article/important-message--security-vulnerability-java-edition
- Mirantis : https://github.com/Mirantis/security/blob/main/news/cve-2021-44288.md
- Miro : https://miro.com/trust/updates/log4j/
- MISP : https://twitter.com/MISPProject/status/1470051242038673412
- Mitel : https://www.mitel.com/support/security-advisories/mitel-product-security-advisory-21-0010
- MOBOTIX : https://www.mobotix.com/index.php/en/no-threat-log4j-all-mobotix-products
- MongoDB : https://www.mongodb.com/blog/post/log4shell-vulnerability-cve-2021-44228-and-mongodb
- MONARC Project : https://twitter.com/MONARCproject/status/1470349937443491851
- Moodle : https://moodle.org/mod/forum/discuss.php?d=429966
- MoogSoft : https://servicedesk.moogsoft.com/hc/en-us/articles/4412463233811?input_string=log4j+vulnerability+%7C%7C+cve-2021-44228
- Motorola Avigilon : https://support.avigilon.com/s/article/Technical-Notification-Apache-Log4j2-vulnerability-impact-on-Avigilon-products-CVE-2021-44228?language=en_US
- Mulesoft : https://help.mulesoft.com/s/article/Apache-Log4j2-vulnerability-December-2021

N
##############################################################################

- N-able : https://www.n-able.com/security-and-privacy/apache-log4j-vulnerability
- Nagios : https://www.nagios.com/news/2021/12/update-on-apache-log4j-vulnerability/
- NAKIVO : https://forum.nakivo.com/index.php?/topic/7574-log4j-cve-2021-44228/&do=findComment&comment=9145
- NELSON : https://github.com/getnelson/nelson/blob/f4d3dd1f1d4f8dfef02487f67aefb9c60ab48bf5/project/custom.scala
- NEO4J : https://community.neo4j.com/t/log4j-cve-mitigation-for-neo4j/48856
- NetApp : https://security.netapp.com/advisory/ntap-20211210-0007/
- NetBox : https://github.com/netbox-community/netbox/discussions/8052#discussioncomment-1796920
- NetBrain (Requires Customer Portal Access) : https://www.netbraintech.com/official-fix-for-log4j2-critical-vulnerability-cve-2021-44228
- Netcup : https://www.netcup-news.de/2021/12/14/pruefung-log4j-sicherheitsluecken-abgeschlossen/
- Netflix : https://github.com/search?q=org%3ANetflix+CVE-2021-44228&type=commits
- NetGate PFSense : https://forum.netgate.com/topic/168417/java-log4j-vulnerability-is-pfsense-affected/35
- Netgear : https://www.reddit.com/r/NETGEAR/comments/re5iqy/comment/ho9qlvb/
- Netwrix : https://www.netwrix.com/netwrix_statement_on_cve_2021_44228_the_apache_log4j_vulnerability.html
- NewTek : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995403
- NextCloud : https://help.nextcloud.com/t/apache-log4j-does-not-affect-nextcloud/129244
- NextGen Healthcare Mirth : https://github.com/nextgenhealthcare/connect/discussions/4892#discussioncomment-1789526
- Nexus Group : https://doc.nexusgroup.com/pages/viewpage.action?pageId=83133294
- Newrelic : https://docs.newrelic.com/docs/security/new-relic-security/security-bulletins/security-bulletin-nr21-03/
- Nice Software (AWS) EnginFRAME : https://download.enginframe.com/
- NinjaRMM : https://ninjarmm.zendesk.com/hc/en-us/articles/4416226194189-12-10-21-Security-Declaration-NinjaOne-not-affected-by-CVE-2021-44228-log4j-
- Nomachine : https://forums.nomachine.com/topic/apache-log4j-notification
- NoviFlow : https://noviflow.com/noviflow-products-and-the-log4shell-exploit-cve-2021-44228/
- NI (National Instruments) : https://www.ni.com/en-us/support/documentation/supplemental/21/ni-response-to-apache-log4j-vulnerability-.html
- NSFocus : https://nsfocusglobal.com/apachelog4j-remote-code-execution-vulnerability-cve-2021-44228-threat-alert/
- Nulab : https://nulab.com/blog/company-news/log4shell/
- Nutanix : https://download.nutanix.com/alerts/Security_Advisory_0023.pdf
- Nvidia : https://nvidia.custhelp.com/app/answers/detail/a_id/5294
- NXLog : https://nxlog.co/news/apache-log4j-vulnerability-cve-2021-44228

O
##############################################################################

- Objectif Lune : https://learn.objectiflune.com/blog/security/statement-on-log4j-vulnerability-cve-2021-4428/
- Obsidiandynamics KAFDROP : https://github.com/obsidiandynamics/kafdrop/issues/315
- OCLC : https://oclc.service-now.com/status
- Octopus : https://advisories.octopus.com/adv/December.2306508680.html
- Okta : https://sec.okta.com/articles/2021/12/log4shell
- Onespan :https://www.onespan.com/remote-code-execution-vulnerability-in-log4j2-cve-2018-11776
- OnlyOffice : https://forum.onlyoffice.com/t/does-onlyoffice-documentserver-uses-log4j/841
- OpenCMS : https://documentation.opencms.org/opencms-documentation/server-installation/log4j-security-vulnerability/index.html
- Opengear : https://opengear.zendesk.com/hc/en-us/articles/4412713339419-CVE-2021-44228-aka-Log4Shell-Opengear-products-are-not-affected
- OpenHab : https://github.com/openhab/openhab-distro/pull/1343
- OpenNMS : https://www.opennms.com/en/blog/2021-12-10-opennms-products-affected-by-apache-log4j-vulnerability-cve-2021-44228/
- OpenMRS TALK : https://talk.openmrs.org/t/urgent-security-advisory-2021-12-11-re-apache-log4j-2/35341
- OpenSearch : https://discuss.opendistrocommunity.dev/t/log4j-patch-for-cve-2021-44228/7950
- OpenText : https://www.opentext.com/support/log4j-remote-code-execution-advisory
- OpenTripPlanner : https://github.com/opentripplanner/OpenTripPlanner/issues/3785
- OpenVPN : https://forums.openvpn.net/viewtopic.php?f=4&p=103724#p103750
- OPNsense : https://forum.opnsense.org/index.php?topic=25951.msg125111#msg125111
- Oracle : https://www.oracle.com/security-alerts/alert-cve-2021-44228.html
- Orgavision : https://www.orgavision.com/neuigkeiten/sicherheitsluecke-java-library-log4j
- OSANO : https://www.osano.com/articles/apache-log4j-vulnerability-update
- Osirium : https://www.osirium.com/blog/apache-log4j-vulnerability
- OSQUERY : https://twitter.com/osquery/status/1470831336118124549
- OTRS : https://portal.otrs.com/external
- OVHCloud : https://blog.ovhcloud.com/log4shell-how-to-protect-my-cloud-workloads/
- OwnCloud : https://central.owncloud.org/t/owncloud-not-directly-affected-by-log4j-vulnerability/35493
- OxygenXML : https://www.oxygenxml.com/security/advisory/CVE-2021-44228.html

P
##############################################################################

- Palantir : https://www.palantir.com/security-advisories/log4j-vulnerability/
- Palisade : https://kb.palisade.com/index.php?pg=kb.printer.friendly&id=3#p1826
- Palo-Alto Networks : https://security.paloaltonetworks.com/CVE-2021-44228
- PandoraFMS : https://pandorafms.com/blog/es/cve-2021-44228/
- Panopto : https://support.panopto.com/s/article/Panopto-Statement-on-the-Log4j2-Zero-Day-Vulnerability
- Pantheon hosting : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3996953
- PaperCut : https://www.papercut.com/kb/Main/Log4Shell-CVE-2021-44228
- Parallels : https://kb.parallels.com/en/128696
- Parse.ly : https://blog.parse.ly/parse-ly-log4shell/
- PasswordSafe : https://www.passwordsafe.com/de/blog/log4j-zero-day-luecke/
- PasswordState : https://www.reddit.com/r/passwordstate/comments/rf7d62/log4j_zeroday_log4shell_vulnerability/
- PDQ : https://www.pdq.com/blog/log4j-vulnerability-cve-2021-44228/
- Pebblehost : https://help.pebblehost.com/en/article/patching-the-log4j-rce-exploit-14wyvz0/
- Percona : https://www.percona.com/blog/log4jshell-vulnerability-update/
- Perforce : https://www.perforce.com/support/log4j
- Pega : https://docs.pega.com/security-advisory/security-advisory-apache-log4j-zero-day-vulnerability
- Pentaho :https://support.pentaho.com/hc/en-us/articles/4416229254541-log4j-2-zero-day-vulnerability-No-impact-to-supported-versions-of-Pentaho-
- Pexip : https://www.pexip.com/blog1.0/pexip-statement-on-log4j-vulnerability
- Phenix Id : https://support.phenixid.se/uncategorized/log4j-fix/
- Philips : https://www.philips.com/a-w/security/security-advisories.html
- PiHole : https://www.reddit.com/r/pihole/comments/re225u/does_pihole_use_log4j/
- Ping Identity : https://support.pingidentity.com/s/article/Log4j2-vulnerability-CVE-CVE-2021-44228
- Pitney Bowes : https://www.pitneybowes.com/us/support/apache-log4j-vulnerability.html
- Planmeca : https://www.planmeca.com/apache-log4j-vulnerability-in-planmeca-products/
- Planon Software : https://my.planonsoftware.com/uk/news/log4j-impact-on-planon/
- Platform.SH : https://platform.sh/blog/2021/platformsh-protects-from-apache-log4j/
- Plesk : https://support.plesk.com/hc/en-us/articles/4412182812818-CVE-2021-44228-vulnerability-in-log4j-package-of-Apache
- Polycom : https://support.polycom.com/content/dam/polycom-support/global/documentation/plygn-21-08-poly-systems-apache.pdf
- Portainer : https://www.portainer.io/blog/portainer-statement-re-log4j-cve-2021-44228
- PortEx : https://github.com/katjahahn/PortEx/releases
- PortSwigger : https://forum.portswigger.net/thread/are-burp-collaborator-or-burp-enterprise-vulnerable-to-log4j-dc6524e0
- PostGreSQL : https://www.postgresql.org/about/news/postgresql-jdbc-and-the-log4j-cve-2371/
- Postman : https://support.postman.com/hc/en-us/articles/4415791933335-Is-Postman-impacted-by-the-Log4j-vulnerability-CVE-2021-44228-
- PowerAdmin : https://www.poweradmin.com/blog/solarwinds-hack-our-safety-measures/
- Precisely : https://customer.precisely.com/s/article/CVE-2021-44228-Log4Shell?language=en_US
- Pretix : https://pretix.eu/about/de/blog/20211213-log4j/
- PrimeKey : https://support.primekey.com/news/posts/information-about-primekey-products-and-log4j-vulnerability-cve-2021-44228
- ProDVX : https://www.prodvx.com/blog/prodvx-statement-on-the-apache-log4j-security-flaw
- Progress / IpSwitch : https://www.progress.com/security
- ProofPoint : https://proofpointcommunities.force.com/community/s/article/Proofpoint-Statement-Regarding-CVE-2021-44228-Java-logging-package-log4j2
- ProSeS : https://www.proses.de/en/2021/12/16/log4shell-cve-2021-44228/
- Prosys : https://prosysopc.com/news/important-security-release/
- ProtonMail : https://twitter.com/ProtonMail/status/1470377648492797953
- Proxmox : https://forum.proxmox.com/threads/log4j-exploit-what-to-do.101254/#post-436880
- PRTG Paessler : https://kb.paessler.com/en/topic/90213-is-prtg-affected-by-cve-2021-44228
- PTC : https://www.ptc.com/en/documents/log4j
- PTV Group : https://company.ptvgroup.com/en/resources/service-support/log4j-latest-information
- Pulse Secure : https://kb.pulsesecure.net/articles/Pulse_Secure_Article/KB44933/?kA13Z000000L3dR
- Puppet : https://puppet.com/blog/puppet-response-to-remote-code-execution-vulnerability-cve-2021-44228/
- Pure Storage : https://support.purestorage.com/Field_Bulletins/Interim_Security_Advisory_Regarding_CVE-2021-44228_(%22log4j%22)
- PWM Project : https://github.com/pwm-project/pwm/issues/628
- Pyramid Analytics : https://community.pyramidanalytics.com/t/83hjjt4/log4j-security-vulnerability-pyramid

Q
##############################################################################

- QF-Test : https://www.qfs.de/en/blog/article/no-log4j-vulnerability-in-qf-test.html
- Qlik : https://community.qlik.com/t5/Support-Updates-Blog/Vulnerability-Testing-Apache-Log4j-reference-CVE-2021-44228-also/ba-p/1869368
- QMATIC : https://www.qmatic.com/meet-qmatic/news/qmatic-statement-on-log4j-vulnerability 
- QNAP : https://www.qnap.com/en-uk/security-advisory/qsa-21-58
- QOPPA : https://kbdeveloper.qoppa.com/cve-2021-44228-apache-log4j-vulnerability/
- QSC Q-SYS : https://qscprod.force.com/selfhelpportal/s/article/Are-Q-SYS-products-affected-by-the-Log4j-vulnerability-CVE-2021-44228
- QT : https://www.qt.io/blog/the-qt-company-products-not-affected-by-cve-2021-44228-log4j-vulnerability
- Qualys : 
   - https://www.qualys.com/was-log4shell-help
   - https://blog.qualys.com/vulnerabilities-threat-research/2021/12/10/apache-log4j2-zero-day-exploited-in-the-wild-log4shell
- Quest Global : https://support.quest.com/fr-fr/search#q=CVE-2021-44228&t=Global

R
##############################################################################

- R2ediviewer : https://r2ediviewer.de/DE/reload.html?Change-log_17858584.html
- Radfak : https://www.radfak.de/ankuendigungen-news/130-radfak-und-log4j-cve-2021-44228-sicherheitsluecke.html
- Radware : https://support.radware.com/app/answers/answer_view/a_id/1029752
- Rapid7 : https://www.rapid7.com/blog/post/2021/12/14/update-on-log4shells-impact-on-rapid7-solutions-and-systems/
- Raritan : https://www.raritan.com/support
- Ravelin : https://syslog.ravelin.com/log4shell-cve-2021-44228-4338bb8da67b
- Red5Pro : https://www.red5pro.com/blog/red5-marked-safe-from-log4j-and-log4j2-zero-day/
- RedGate : https://www.red-gate.com/privacy-and-security/vulnerabilities/2021-12-15-log4j-statement
- RedHat : https://access.redhat.com/security/vulnerabilities/RHSB-2021-009
- Redis : https://redis.com/security/notice-apache-log4j2-cve-2021-44228/
- Reiner SCT : https://forum.reiner-sct.com/index.php?/topic/5973-timecard-und-log4j-schwachstelle/&do=findComment&comment=14933
- Remediant : https://twitter.com/Remediant/status/1470278329454366720
- ReportURI : https://scotthelme.co.uk/responding-to-the-log4j-2-vulnerability/
- ResMed : https://www.resmed.com/en-us/security/
- Respondus : https://support.respondus.com/support/index.php?/News/NewsItem/View/339
- Revenera / Flexera : https://community.flexera.com/t5/Revenera-Company-News/Security-Advisory-Log4j-Java-Vulnerability-CVE-2021-44228/ba-p/216905
- Ricoh : https://www.ricoh.com/info/2021/1215_1/
- RingCentral : https://www.ringcentral.com/trust-center/security-bulletin.html
- Riverbed : https://supportkb.riverbed.com/support/index?page=content&id=S35645
- RocketChat : https://github.com/RocketChat/Rocket.Chat/issues/23927
- Rocket Software : https://community.rocketsoftware.com/forums/forum-home/digestviewer/viewthread?MessageKey=4f7520d4-ebdf-46be-ae93-60ec058d6baa&CommunityKey=dd45d00d-59db-4884-b3eb-2b0647af231b&tab=digestviewer&bm=4f7520d4-ebdf-46be-ae93-60ec058d6baa#bm4f7520d4-ebdf-46be-ae93-60ec058d6baa
- Rockwell Automation : https://rockwellautomation.custhelp.com/app/answers/answer_view/a_id/1133605
- Rollbar : https://rollbar.com/blog/log4j-zero-day-2021-log4shell/
- Rosetta UNOFICIAL : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3993903
- Rosette.com : https://support.rosette.com/hc/en-us/articles/4416216525965-Log4j-Vulnerability
- Royal HaskoningDHV Digital : https://www.lanner.com/fr-fr/insights/news/royal-haskoningdhv-digital-and-cve-2021-44228-apache-log4j2.html
- Rubrik : https://support.rubrik.com/s/announcementdetail?Id=a406f000001PwOcAAK
- Ruckus wireless : https://support.ruckuswireless.com/security_bulletins/313
- Runecast : https://www.runecast.com/blog/runecast-6-0-1-0-covers-apache-log4j-java-vulnerability
- RunDeck by PagerDuty : https://docs.rundeck.com/docs/history/CVEs/
- RSA SecurID: https://community.rsa.com/t5/general-security-advisories-and/rsa-customer-advisory-apache-vulnerability-log4j2-cve-2021-44228/ta-p/660501
- RSA Netwitness : https://community.rsa.com/t5/netwitness-platform-product/netwitness-apache-vulnerability-log4j2-cve-2021-44228-nbsp/ta-p/660540

S
##############################################################################

- SAE-IT : https://www.sae-it.com/nc/de/news/sicherheitsmeldungen.html
- SAFE FME Server : https://community.safe.com/s/article/Is-FME-Server-Affected-by-the-Security-Vulnerability-Reported-Against-log4j
- SAGE : https://www.sagecity.com/sage-global-solutions/sage-crm/f/sage-crm-announcements-news-and-alerts/178655/advisory-apache-log4j-vulnerability-cve-2021-44228
- SailPoint : https://community.sailpoint.com/t5/IdentityIQ-Blog/IdentityIQ-log4j-Remote-Code-Execution-Vulnerability/ba-p/206681
- Salesforce : https://help.salesforce.com/s/articleView?id=000363736&type=1
- Sangoma :https://help.sangoma.com/community/s/article/Log4Shell
- SAP Advanced Platform : https://launchpad.support.sap.com/#/notes/3130698
- SAP BusinessObjects : https://launchpad.support.sap.com/#/notes/3129956
- SAP Global coverage : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3994039
- SAS : https://support.sas.com/content/support/en/security-bulletins/remote-code-execution-vulnerability-cve-2021-44228.html
- SASSAFRAS : https://www.sassafras.com/log4j-vulnerability-cve-2021-44228/
- Savignano software solutions : https://savignano.atlassian.net/wiki/spaces/SNOTIFY/blog/2021/12/13/2839740417/No+Log4j+Vulnerability+in+S+Notify
- ScaleComputing : https://community.scalecomputing.com/s/article/Apache-Log4j-Vulnerability
- ScaleFusion MobileLock Pro : https://help.mobilock.in/article/t9sx43yg44-scalefusion-security-advisory-for-apache-log-4-j-vulnerability-cve-2021-44228
- Scalingo : https://scalingo.com/blog/cve-2021-44228-log4shell
- Schneider Electric : https://download.schneider-electric.com/files?p_Doc_Ref=SESB-2021-347-01
- SCM Manager : https://scm-manager.org/blog/posts/2021-12-13-log4shell/
- ScreenBeam : https://customersupport.screenbeam.com/hc/en-us/articles/4416468085389-December-2021-Security-Alert-Log4j-CVE-2021-44228
- SDL worldServer : https://gateway.sdl.com/apex/communityknowledge?articleName=000017707
- Seafile : https://forum.seafile.com/t/urgent-zero-day-exploit-in-log4j/15575
- Seagull Scientific : https://support.seagullscientific.com/hc/en-us/articles/4415794235543-Apache-Log4Shell-Vulnerability
- SecurePoint : https://www.securepoint.de/news/details/sicherheitsluecke-log4j-securepoint-loesungen-nicht-betroffen.html
- Security Onion : https://blog.securityonion.net/2021/12/security-onion-2390-20211210-hotfix-now.html 
- Seeburger : https://servicedesk.seeburger.de/portal/en-US/Knowledge/Article/?defId=101040&id=25486312&COMMAND=Open
- SentinelOne : https://www.sentinelone.com/blog/cve-2021-44228-staying-secure-apache-log4j-vulnerability/
- Sentry : https://blog.sentry.io/2021/12/15/sentrys-response-to-log4j-vulnerability-cve-2021-44228
- SEP : https://support.sep.de/otrs/public.pl?Action=PublicFAQZoom;ItemID=132
- Server Eye : https://www.server-eye.de/blog/sicherheitsluecke-log4j-server-eye-systeme-sind-nicht-betroffen/
- ServiceNow : https://support.servicenow.com/kb?id=kb_article_view&sysparm_article=KB1000959
- Sesam Info : https://twitter.com/sesam_info/status/1469711992122486791
- SFIRM : https://www.sfirm.de/nc/aktuelle-meldungen/aktuelles/article//update-141221-sfirm-440-von-java-sicherheitsluecke-log4j-nicht-betroffen.html
- Shibboleth : http://shibboleth.net/pipermail/announce/2021-December/000253.html
- Shopify : https://community.shopify.com/c/technical-q-a/is-shopify-affected-by-the-log4j-vulnerability/td-p/1417625
- Siebel : https://www.siebelhub.com/main/2021/12/log4j-vulnerability-cve-2021-44228-and-siebel-crm.html
- Siemens : https://cert-portal.siemens.com/productcert/pdf/ssa-661247.pdf
- Siemens Healthineers : https://www.siemens-healthineers.com/support-documentation/cybersecurity/cve-2021-44228
- Sierra Wireless : https://source.sierrawireless.com/resources/security-bulletins/sierra-wireless-technical-bulletin---swi-psa-2021-007/
- Signald : https://gitlab.com/signald/signald/-/issues/259
- SingleWire : https://support.singlewire.com/s/article/Apache-Log4j2-vulnerability-CVE-2021-44228
- Sitecore : https://support.sitecore.com/kb?id=kb_article_view&sysparm_article=KB1001391
- SiteGround : https://twitter.com/SiteGround/status/1470334089500798976
- Skillable : https://skillable.com/log4shell/
- SLF4J : http://slf4j.org/log4shell.html
- SmartBear : https://smartbear.com/security/cve-2021-44228/
- SmileCDR : https://www.smilecdr.com/our-blog/a-statement-on-log4shell-cve-2021-44228
- Sn0m : https://www.snom.com/en/press/log4j-poses-no-threat-snom-phones/
- Snakemake : https://snakemake.readthedocs.io/en/stable/
- Snow Software : https://community.snowsoftware.com/s/article/Vulnerability-in-Log4j-CVE-2021-45105
- Snowflake : https://community.snowflake.com/s/article/No-Snowflake-exposure-to-Apache-Log4j-vulnerability-CVE-2021-44228
- Snyk : https://updates.snyk.io/snyk%27s-cloud-platform-all-clear-from-log4j-exploits-216499
- Spigot : https://www.spigotmc.org/threads/spigot-security-releases-%E2%80%94-1-8-8%E2%80%931-18.537204/
- Software AG : https://tech.forums.softwareag.com/t/log4j-zero-day-vulnerability/253849
- Solace : https://solace.community/discussion/1131/solace-issue-notification-sol-61111-cve-2021-44228-cve-2021-45046-apache-log4j-jndi-vulnerability
- SolarWinds : https://www.solarwinds.com/trust-center/security-advisories/cve-2021-44228
- SonarSource : https://community.sonarsource.com/t/sonarqube-and-the-log4j-vulnerability/54721
- Sonatype : https://blog.sonatype.com/a-new-0-day-log4j-vulnerability-discovered-in-the-wild
- SonicWall : https://psirt.global.sonicwall.com/vuln-detail/SNWLID-2021-0032
- Sophos : https://www.sophos.com/en-us/security-advisories/sophos-sa-20211210-log4j-rce
- SOS Berlin : https://www.sos-berlin.com/en/news-mitigation-log4j-vulnerability
- SpaceLabs Healthcare : https://www.spacelabshealthcare.com/products/security/security-advisories-and-archives/log4shell-vulnerability-assessment-and-potential-product-impact-statement/
- Spambrella : https://www.spambrella.com/faq/status-of-spambrella-products-with-cve-2021-44228/
- Sprecher Automation : https://www.sprecher-automation.com/en/it-security/security-alerts
- Splashtop : https://support-splashtopbusiness.splashtop.com/hc/en-us/articles/4412788262811-Is-Splashtop-affected-by-Apache-Log4j-
- Splunk : https://www.splunk.com/en_us/blog/bulletins/splunk-security-advisory-for-apache-log4j-cve-2021-44228.html
- Spring Boot : https://spring.io/blog/2021/12/10/log4j2-vulnerability-and-spring-boot
- StarDog : https://community.stardog.com/t/stardog-7-8-1-available/3411
- StarWind : https://www.starwindsoftware.com/security/apache_log4j/
- STERIS : https://h-isac.org/wp-content/uploads/2021/12/Steris_Revised-Security-Advisory-For-Apaches-Log4j-12.16.21.pdf
- Sterling Order IBM : https://www.ibm.com/support/pages/node/6525544
- Storagement : https://www.storagement.de/index.php?action=topicofthemonth&site=log4j
- StormShield : https://www.stormshield.com/news/log4shell-security-alert-stormshield-product-response/
- StrangeBee TheHive & Cortex : https://blog.strangebee.com/apache-log4j-cve-2021-44228/
- Stratodesk : http://cdn.stratodesk.com/repository/notouch-center/10/4.5.231/0/ReleaseNotes-Stratodesk-NoTouch_Center-4.5.231.html
- Strimzi : https://strimzi.io/blog/2021/12/14/strimzi-and-log4shell/
- Stripe : https://support.stripe.com/questions/update-for-apache-log4j-vulnerability-(cve-2021-44228)
- Styra : https://blog.styra.com/blog/newest-log4j-security-vulnerability-cve-2021-44228-log4shell
- SumoLogic : https://help.sumologic.com/Release-Notes/Collector-Release-Notes#december-11-2021-19-361-12
- Superna EYEGLASS : https://manuals.supernaeyeglass.com/project-technical-advisories-all-products/HTML/technical-advisories.html#h2__1912345025
- Suprema Inc : https://www.supremainc.com/en/
- Surepoint : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3998301
- SUSE : https://www.suse.com/c/suse-statement-on-log4j-log4shell-cve-2021-44228-vulnerability/
- Sweepwidget : https://sweepwidget.com/view/23032-v9f40ns1/4zow83-23032
- Swingset : https://github.com/bpangburn/swingset/blob/017452b2d0d8370871f43a68043dacf53af7f759/swingset/CHANGELOG.txt#L10
- Swisslog : https://www.swisslog.com/en-us/about-swisslog/newsroom/news-press-releases-blog-posts/2021/12/apache-cyber-attack
- Swyx : https://service.swyx.net/hc/de/articles/4412323539474
- Syncplify : https://blog.syncplify.com/no-we-are-not-affected-by-log4j-vulnerability/
- Synchro MSP : https://community.syncromsp.com/t/log4j-rce-cve-2021-4428/1350
- Synology : https://www.synology.com/en-global/security/advisory/Synology_SA_21_30
- Synopsys : https://community.synopsys.com/s/article/SIG-Security-Advisory-for-Apache-Log4J2-CVE-2021-44228
- Syntevo : https://www.syntevo.com/blog/?p=5240
- SysAid : https://www.sysaid.com/lp/important-update-regarding-apache-log4j
- Sysdig : https://sysdig.com/blog/cve-critical-vulnerability-log4j/

T
##############################################################################

- Tableau server (Now a Salesforce company!): https://kb.tableau.com/articles/issue/Apache-Log4j2-vulnerability-Log4shell
- Talend : 
   - https://jira.talendforge.org/browse/TCOMP-2054
   - https://www.talend.com/security/incident-response/
   - https://help.talend.com/r/EeTpT8r7xmeq1HtTGQBqGA/zX7iWLX6GgxOAjJPlpXNYA
- Tanium : https://community.tanium.com/s/article/How-Tanium-Can-Help-with-CVE-2021-44228-Log4Shell#_Toc90296319
- Targit : https://github.com/NCSC-NL/log4shell/blob/main/software/vendor-statements/Targit.png
- Tasktop : https://docs.tasktop.com/home/cve-2021-44228-apache-log4j-vulnerability-in-tasktop-products
- TealiumIQ : https://community.tealiumiq.com/t5/Announcements-Blog/Update-on-Log4j-Security-Vulnerability/ba-p/36824
- TeamPasswordManager : https://teampasswordmanager.com/blog/log4j-vulnerability/
- Teamviewer : https://www.teamviewer.com/en/trust-center/security-bulletins/hotfix-log4j2-issue/
- TechSmith : https://support.techsmith.com/hc/en-us/articles/4416620527885?input_string=log4j
- Tenable : https://www.tenable.com/log4j
- Telestream : http://www.telestream.net/telestream-support/Apache-Log4j2-Bulletin.htm
- Teradici : https://support.hp.com/us-en/document/ish_5268006-5268030-16
- Terra : https://terra.bio/terras-security-response-to-the-log4j-vulnerability/
- TestOut : https://support.testout.com/hc/en-us/articles/4413081889947-Is-TestOut-LabSim-Impacted-by-the-Apache-Log4j-vulnerability-CVE-2021-44228-
- Thales : https://supportportal.thalesgroup.com/csm?id=kb_article_view&sys_kb_id=02863d13db544110f0e3220805961914&sysparm_article=KB0025287
- Thales (SafeNet) HSM : https://supportportal.thalesgroup.com/csm?id=kb_article_protected&sys_id=12acaed3dbd841105d310573f3961953
- The Access Group : https://pages.theaccessgroup.com/Response-Log4J-Dec21.html
- Therefore : https://therefore.net/log4j-therefore-unaffected/
- Thermo Fisher : https://corporate.thermofisher.com/us/en/index/about/information-security/Protecting-Our-Products.html
- ThreatLocker : https://threatlocker.kb.help/log4j-vulnerability/
- Threema UNOFICIAL : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3993316
- ThycoticCentrify : https://docs.thycotic.com/bulletins/current/2021/cve-2021-44228-exploit.md
- Tibco : https://www.tibco.com/support/notices/2021/12/apache-log4j-vulnerability-update
- TopDesk : https://my.topdesk.com/tas/public/ssp/content/detail/knowledgeitem?unid=74952771dfab4b0794292e63b0409314
- Top Gun Technology (TGT) : https://www.topgun-tech.com/technical-bulletin-apache-software-log4j-security-vulnerability-cve-2021-44228/
- Topicus KeyHub : https://blog.topicus-keyhub.com/topicus-keyhub-is-not-vulnerable-to-cve-2021-44228/
- Topix : https://www.topix.de/de/technik/systemfreigaben.html
- Tosibox : https://helpdesk.tosibox.com/support/solutions/articles/2100050946-security-advisory-on-vulnerability-in-apache-log4j-library-cve-2021-44228
- TP-Link : https://www.tp-link.com/jp/support/faq/3255/
- TrendMicro : 
   - https://success.trendmicro.com/solution/000289940
   - https://www.trendmicro.com/en_us/research/21/l/patch-now-apache-log4j-vulnerability-called-log4shell-being-acti.html
- Tricentis Tosca : https://support-hub.tricentis.com/open?number=NEW0001148&id=post
- Tripwire : https://www.tripwire.com/log4j
- TrueNAS : https://www.truenas.com/community/threads/log4j-vulnerability.97359/post-672559
- Tufin : https://portal.tufin.com/articles/SecurityAdvisories/Apache-Log4Shell-Vulnerability-12-12-2021
- TYPO3 : https://typo3.org/article/typo3-psa-2021-004

U
##############################################################################

- Ubiquiti-UniFi-UI : https://community.ui.com/releases/UniFi-Network-Application-6-5-55/48c64137-4a4a-41f7-b7e4-3bee505ae16e
- Ubuntu : https://ubuntu.com/security/CVE-2021-44228
- Umbraco : https://umbraco.com/blog/security-advisory-december-15-2021-umbraco-cms-and-cloud-not-affected-by-cve-2021-44228-log4j-rce-0-day-mitigation/
- Unify ATOS : https://networks.unify.com/security/advisories/OBSO-2112-01.pdf
- UniFlow : https://www.uniflow.global/en/security/security-and-maintenance/
- Unimus : https://forum.unimus.net/viewtopic.php?f=7&t=1390#top
- USSIGNAL MSP : https://ussignal.com/blog/apache-log4j-vulnerability

V
##############################################################################

- VArmour : https://support.varmour.com/hc/en-us/articles/4416396248717-Log4j2-Emergency-Configuration-Change-for-Critical-Auth-Free-Code-Execution-in-Logging-Utility
- Varian : https://www.varian.com/resources-support/services/cybersecurity-varian/java-log4j-vulnerabilities
- Varonis : https://help.varonis.com/s/article/Apache-Log4j-Zero-Day-Vulnerability-CVE-2021-44228
- Varnish Software : https://docs.varnish-software.com/security/CVE-2021-44228-45046/
- Vector : https://www.vector.com/cl/en/support-downloads/security-advisories/log4j/#c248921
- Veeam : https://www.veeam.com/kb4254
- VLC : https://www.videolan.org/news.html#news-2021-12-15
- Venafi : https://support.venafi.com/hc/en-us/articles/4416213022733-Log4j-Zero-Day-Vulnerability-notice
- Vendavo : https://www.vendavo.com/all/latest-vendavo-response-to-log4shell-vulnerability/
- Veritas NetBackup : https://www.veritas.com/content/support/en_US/article.100052070
- Vertica : https://forum.vertica.com/discussion/242512/vertica-security-bulletin-a-potential-vulnerability-has-been-identified-apache-log4j-library-used
- Vertiv : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3996968
- VertX : https://vertx.io/blog/CVE-2021-44228/
- Vespa ENGINE : https://github.com/vespa-engine/blog/blob/f281ce4399ed3e97b4fed32fcc36f9ba4b17b1e2/_posts/2021-12-10-log4j-vulnerability.md 
- Vigilant Software (CyberComply : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3997784
- VIPRE : https://labs.vipre.com/security-advisory-vipres-analysis-of-critical-log4j-vulnerability/
- Viso Trust : https://blog.visotrust.com/viso-trust-statement-re-cve-2021-44228-log4j-a4b9b5767492
- VisualSVN : https://www.visualsvn.com/company/news/visualsvn-products-are-not-affected-by-CVE-2021-44228
- VMware : https://www.vmware.com/security/advisories/VMSA-2021-0028.html
   - VMware vCenter : 
      - https://kb.vmware.com/s/article/87088
      - https://angrysysops.com/2021/12/17/additional-step-for-vcenter-server-cve-2021-44228-and-cve-2021-45046-workaround/
- VTScada : https://www.vtscada.com/vtscada-unaffected-by-log4j/
- Vyaire : https://www.vyaire.com/product-security

W
##############################################################################

- Wallarm : https://lab.wallarm.com/cve-2021-44228-mitigation-update/
- WAPT : https://www.reddit.com/r/WAPT/comments/rg38o9/wapt_is_not_affected_by_the_cve202144228_flaw/
- Wasp Barcode technologies : https://support.waspbarcode.com/kb/articles/assetcloud-inventorycloud-are-they-affected-by-the-java-exploit-log4j-no
- Watcher : https://twitter.com/felix_hrn/status/1470387338001977344
- WatchGuard / Secplicity / https://www.secplicity.org/2021/12/10/critical-rce-vulnerability-in-log4js/
- Western Digital : https://www.westerndigital.com/support/product-security/wdc-21016-apache-log4j-2-remote-code-execution-vulnerability-analysis
- WeblateOrg : https://github.com/WeblateOrg/weblate/issues/6972#issuecomment-992292650
- WildFlyAS : https://twitter.com/WildFlyAS/status/1469362190536818688
- WindRiver : https://support2.windriver.com/index.php?page=security-notices&on=view&id=7191
- WireShark : https://gitlab.com/wireshark/wireshark/-/issues/17783
- Wistia : https://status.wistia.com/incidents/jtg0dfl5l224
- WitFoo : https://www.witfoo.com/blog/emergency-update-for-cve-2021-44228-log4j/
- Wodby Cloud : https://twitter.com/wodbycloud/status/1470125735914450950
- VoiceThread : https://twitter.com/voicethread/status/1470498119540514821
- WordPress : https://wordpress.org/support/topic/is-the-log4j-vulnerability-an-issue/
- Workday : https://blog.workday.com/en-us/2021/workday-response-on-log4j.html
- Worksphere : https://www.worksphere.com/product/security-update-on-log4j-cve-2021-44228
- World Programming WPS analytics : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995649
- Wowza : https://www.wowza.com/docs/known-issues-with-wowza-streaming-engine#log4j2-cve
- WSO2 : https://github.com/wso2/security-tools/pull/169


X
##############################################################################

- XCP-ng : https://xcp-ng.org/forum/topic/5315/log4j-vulnerability-impact
- XenForo : https://xenforo.com/community/threads/psa-potential-security-vulnerability-in-elasticsearch-5-via-apache-log4j-log4shell.201145/
- Xerox : https://security.business.xerox.com/wp-content/uploads/2021/12/Xerox-Special-Bulletin-Regarding-CVE-2021-44228.pdf
- Xilinx : https://support.xilinx.com/s/article/76957?language=en_US
- Xmind : https://support.xmind.net/hc/en-us/community/posts/4412509904537
- XPertDoc : https://kb.xpertdoc.com/pages/viewpage.action?pageId=87622727
- XPLG : https://www.xplg.com/log4j-vulnerability-exploit-log4shell-xplg-secure/
- Xray connector plugin : https://github.com/jenkinsci/xray-connector-plugin/issues/53
- XWIKI : https://forum.xwiki.org/t/log4j-cve-2021-44228-log4shell-zero-day-vulnerability/9557
- Xylem : https://www.xylem.com/siteassets/about-xylem/cybersecurity/advisories/xylem-apache-log4j-xpsa-2021-005.pdf

Y
##############################################################################

- Yandex-Cloud : https://github.com/yandex-cloud/docs/blob/6ff6c676787756e7dd6101c53b051e4cd04b3e85/ru/overview/security-bulletins/index.md#10122021--cve-2021-44228--%D1%83%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%BD%D0%BE%D0%B5-%D0%B2%D1%8B%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%BE%D0%B4%D0%B0-log4shell-apache-log4j
- Yellowbrick : https://support.yellowbrick.com/hc/en-us/articles/4412586575379-Security-Advisory-Yellowbrick-is-NOT-Affected-by-the-Log4Shell-Vulnerability
- YellowFin : https://community.yellowfinbi.com/announcement/notice-critical-vulnerability-in-log4j2
- YSoft SAFEQ : https://www.ysoft.com/getattachment/Products/Security/Standards-Compliance/text/Information-Security-Policy-Statement/YSOFT-SAFEQ-LOG4J-VULNERABILITY-PRODUCT-UPDATE-WORKAROUND-1.pdf

Z
##############################################################################

- Zabbix : https://blog.zabbix.com/zabbix-not-affected-by-the-log4j-exploit/17873/
- ZAMMAD : https://community.zammad.org/t/cve-2021-44228-elasticsearch-users-be-aware/8256
- Zapier : https://community.zapier.com/general-questions-3/log4j-vulnerability-and-zapier-13150
- Zaproxy : https://www.zaproxy.org/blog/2021-12-10-zap-and-log4shell/ 
- Zebra : https://www.zebra.com/us/en/support-downloads/lifeguard-security/cve-2021-442280-dubbed-log4shell-or-logjam-vulnerability.html
- Zellis : https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592#gistcomment-3995461
- Zendesk : https://support.zendesk.com/hc/en-us/articles/4413583476122
- Zenoss : https://support.zenoss.com/hc/en-us
- Zerto : https://help.zerto.com/kb/000004822
- Zesty : https://www.zesty.io/mindshare/company-announcements/log4j-exploit/
- Zimbra : https://bugzilla.zimbra.com/show_bug.cgi?id=109428
- ZPE systems Inc : https://support.zpesystems.com/portal/en/kb/articles/is-nodegrid-os-and-zpe-cloud-affected-by-cve-2021-44228-apache-log4j
- Zoom : https://community.zoom.com/t5/Community-Help-Center/Zoom-security-exposure/m-p/28109/highlight/true#M2059
- ZoomInfo : https://engineering.zoominfo.com/zoominfo-update-on-apache-log4j-vulnerability
- Zowe : https://github.com/zowe/community/issues/1381
- ZSCALER : https://www.zscaler.fr/blogs/security-research/security-advisory-log4j-0-day-remote-code-execution-vulnerability-cve-2021
- Zyxel : https://www.zyxel.com/support/Zyxel_security_advisory_for_Apache_Log4j_RCE_vulnerability.shtml


ndaal - Vulnerabilities - log4j - Other great resources
------------------------------------------------------------------------------

Many thanks to these great resources !

- BlueTeam CheatSheet * Log4Shell* [1]_ 
- Health-ISAC organization [5]_ 
- Royce Williams list sorted by vendors responses [Royce List] [2]_ 
- The list maintained by U.S. Cybersecurity and Infrastructure Security Agency [CISA List] [3]_ 
- Very detailed list [NCSC-NL] [4]_ 

.....

.. Rubric:: Footnotes

.. [1]
   BlueTeam CheatSheet * Log4Shell*
   https://gist.github.com/SwitHak/b66db3a06c2955a9cb71a8718970c592

.. [2]
   Royce Williams list sorted by vendors responses [Royce List]
   https://www.techsolvency.com/story-so-far/cve-2021-44228-log4j-log4shell/

.. [3]
   The list maintained by U.S. Cybersecurity and Infrastructure Security Agency [CISA List]
   https://github.com/cisagov/log4j-affected-db

.. [4]
   Very detailed list [NCSC-NL]
   https://github.com/NCSC-NL/log4shell/blob/main/software/README.md#software-overview

.. [5]
   Health-ISAC
   https://h-isac.org/apache-log4j-notices/

