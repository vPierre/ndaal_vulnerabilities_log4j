Bibliography
=============

**Below are all resources cited throughout the document's footnotes, in order of appearance:**

#. https://semver.org/spec/v2.0.0.html

#. https://creativecommons.org/licenses/by/3.0/

#. https://nvd.nist.gov/vuln/detail/CVE-2021-44228

#. https://github.com/fullhunt/log4j-scan

#. https://github.com/Neo23x0/log4shell-detector

#. https://github.com/NorthwaveSecurity/log4jcheck

#. https://github.com/hillu/local-log4j-vuln-scanner

#. https://github.com/adilsoybali/Log4j-RCE-Scanner

#. https://github.com/takito1812/log4j-detect

#. https://github.com/Diverto/nse-log4shell

#. https://nvd.nist.gov/vuln/detail/CVE-2021-44228