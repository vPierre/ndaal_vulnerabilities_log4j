.. raw:: latex

    \chapter*{At a glance}
    \phantomsection
    \addcontentsline{toc}{chapter}{At a glance}
    \pagenumbering{roman}

..
    The raw piece of code above is to be added at the beginning of any document where you would like the automation to begin Roman page numbering. 


.. csv-table:: At a glance
   :file: /_finalchapter/glance.csv
   :widths: 30, 70
   :header-rows: 1




