******************************************************************************
ndaal - Vulnerabilities - log4j - Germany - German
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Overview:

hier befinden sich nur Hinweise für Deutschland oder Deutschsprachige.

Zusammenfassung
------------------------------------------------------------------------------

Log4Shell bezeichnet eine Schwachstelle in der weit verbreiteten 
Log4j-Java-Protokollierungsbibliothek.
Die Schwachstelle wird unter CVE-2021-44228 geführt.

Hessische Beauftragte für Datenschutz und Informationsfreiheit
------------------------------------------------------------------------------

Der Hessische Beauftragte für Datenschutz und Informationsfreiheit (HBDI) 
informiert über den akuten Handlungsbedarf bezüglich der Schwachstelle in der 
Java-Bibliothek Log4j (Log4j). 

Aufgrund Cyber-Sicherheitswarnung der Warnstufe Rot des Bundesamtes für 
Sicherheit in der Informationstechnik (BSI) informiert der HBDI seinerseits 
wegen der großen Tragweite und Kritikalität über die Schwachstelle in 
Log4j 

- CVE-2021-44228 [1]_ , 
- CVE-2021-45046 [2]_ and 
- CVE-2021-45105 [3]_ . 

Die IT-Bedrohungslage ist deswegen besonders hoch, weil eine hohe Zahl von 
IT-Systemen betroffen sind und weil sich die 
Schwachstelle Log4Shell über das Internet mit geringem technischen Aufwand 
ausnutzen lässt. Nach den am Wochenende durch das BSI beobachteten 
weltweiten Massenscans liegen mittlerweile auch Informationen über 
erfolgreiche Kompromittierungen vor. Es ist zu erwarten, dass Aktivitäten von
Angreifern unter Ausnutzung der Schwachstelle in den nächsten Tagen deutlich
zunehmen werden und das Risiko einer Kompromittierung sehr hoch ist.

Das bedeutet, dass die verantwortlichen Stellen von einer realen, 
unmittelbaren und erheblichen Gefährdung betroffener Systeme und Dienste 
ausgehen müssen, so dass dringender Handlungsbedarf besteht. Aufgrund der 
hohen Anzahl der bisher bekannten, betroffenen Softwareprodukte, IT-Systeme 
und -Dienste, besteht ein akutes Risiko der Verletzung des Schutzes der 
mittels dieser verarbeiteten, personenbezogenen Daten.

Beim Vorliegen der Schwachstelle in Log4j ist die Sicherheit der 
Verarbeitung gemäß Art. 32 DS-GVO für die betroffenen Systeme und ggf. 
darüber hinaus nicht mehr in vollem Umfang gewährleistet. Es liegt in der 
Verantwortung der Verantwortlichen die Sicherheit der Verarbeitung 
wiederherzustellen. Demensprechend empfiehlt der HBDI dringend, dass
Verantwortliche sich über die Schwachstelle informieren, betroffene 
IT-Systeme und -Dienste identifizieren und die erforderlichen Maßnahmen 
ergreifen, Auftragsverarbeiter von sich aus aktiv werden, angemessene 
Maßnahmen ergreifen sowie auf betroffene Verantwortliche zugehen,
Hersteller ihre Produkte auf das Vorhandensein der Schwachstelle prüfen, 
aktiv betroffene Kunden informieren, Handlungsempfehlungen erarbeiten und 
zur Verfügung stellen sowie Updates, Patches oder vergleichbares zur 
Behebung der Schwachstelle entwickeln und bereitstellen.
Das Schließen der Schwachstelle ist hierbei nicht ausreichend. Die 
Verantwortlichen müssen zusätzlich überprüfen, ob es bereits zu 
erfolgreichen Angriffen gekommen ist. In diesem Fall sind entsprechen 
weiterführende Maßnahmen zu ergreifen und zu prüfen ob eine Meldung von 
Verletzungen des Schutzes personenbezogener Daten gemäß Art. 33 DS-GVO 
beim HBDI erfolgen muss.

Bei Log4j handelt es sich um eine Hilfskomponente, die weltweit in vielen 
Java-Anwendungen verwendet wird. Sie dient den betroffenen Anwendungen zur 
Ereignisprotokollierung (Logging). Verwendet wird Log4j nicht nur in 
eigenverantwortlich entwickelten und betriebenen Unternehmensanwendungen, 
sondern ist integraler Bestandteil einer Vielzahl von Software-Produkten und 
IT-Geräten unterschiedlicher Hersteller und Dienstleister. Aufgrund des 
Anwendungszwecks von Logging kann Log4j auch als nicht unmittelbar erkennbare 
Sub-Komponente zum Einsatz kommen.

Aktuelle Informationen und Handlungsempfehlungen zu der Log4j-Schwachstelle 
finden Sie in der Cybersicherheitswarnung des BSI.

BSI
------------------------------------------------------------------------------

Diese Seite wird laufend aktualisiert. [12]_ 

- Aktuelle Fassung der Cyber-Sicherheitswarnung
  Neu: Version 1.0: Kritische "Log4Shell" Schwachstelle in weit verbreiteter 
  Protokollierungsbibliothek Log4j [13]_ 

- Detektion und Reaktion
  Arbeitspapier Detektion und Reaktion Log4j Schwachstelle, Version 1.3 [14]_ 
  Arbeitspapier Detektion und Reaktion Log4j Schwachstelle, Version 1.4 [20]_ 

- Meldungen des BSI
  Version 1.5: Kritische Schwachstelle in log4j veröffentlicht [15]_ 

- Pressemitteilung vom 11. Dezember 2021 [16]_ 

- Pressekonferenz am 13.12.2021, 15 Uhr (Stream der Tagesschau) [17]_ 

- Empfehlungen für Verbraucherinnen und Verbraucher
  Sicherheitslücke "Log4Shell" gefährdet Systeme weltweit [18]_ 

Weiterführende Links
------------------------------------------------------------------------------

- BSI Arbeitspapier zur Detektion und Reaktion zu Log4Shell [3]_ 
- BSI Cyber-Sicherheitswarnung zu Log4Shell: [4]_ 
- BSI Pressemitteilung zu Log4Shell: [5]_ 
- BayLDA Meldung einer Datenschutzverletzung nach Art. 33 DS-GVO für bayerische
  Verantwortliche aus dem nicht-öffentlichen Bereich: [6]_ 
- BayLDA Cybersicherheit Checkliste mit Prüfkriterien nach Art. 32 DS-GVO: [7]_ 
- BayLDA Patch Management Checkliste nach Art. 32 DS-GVO: [8]_ 
- Cyberabwehr Bayern - Ansprechpartner zur Cybersicherheit in Bayern: [9]_ 
- Zentrale Ansprechstelle Cybercrime (ZAC) des Bayerischen 
  Landeskriminalamtes (LKA): [10]_ 

Hinweise
------------------------------------------------------------------------------

.. Admonition:: Hinweis
   :class: Note

   Die Log4j Version 2.12.2 ist nicht von der Schwachstelle betroffen, obwohl 
   sie sich in der oben genannten Spanne von Versionsnummern befindet. Diese 
   Version dient der Kompatibilität mit Java Version 7.

.. Admonition:: Hinweis
   :class: Note

   Dieses Update können i.d.R. nur die Softwarehersteller vornehmen, die die Bibliothek in ihre
   Programme eingebunden haben. Dafür müssen die Updates des jeweiligen Programms von den
   Administratoren installiert werden. Das alleinige Aktualisieren der Bibliothek über die
   Softwareverwaltung von Betriebssystemen, wie das zum Beispiel in vielen Linux-Distributionen
   möglich ist, reicht zum Schließen der Schwachstelle nicht aus. Ebenfalls ist – entgegen anderslautender
   Berichte – ein Update von Java selbst kein Mittel zur Mitigation. [21]_ 

.. Admonition:: Hinweis
   :class: Note

   Achtung: Es ist möglich, dass die Bibliothek in Anwendungen auf eine Weise verwendet wird, die die
   folgende Konfigurationsanpassung unwirksam macht [22]_ 

.....

.. Rubric:: Footnotes

.. [1]
   CVE-2021-44228
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228

.. [2]
   CVE-2021-45046
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046

.. [3]
   BSI Arbeitspapier zur Detektion und Reaktion zu Log4Shell:
   https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/Vorfaelle/log4jSchwachstelle-2021/log4j_Schwachstelle_Detektion_Reaktion.pdf?__blob=publicationFile&v=2

.. [4]
   BSI Cyber-Sicherheitswarnung zu Log4Shell:
   https://www.bsi.bund.de/SharedDocs/Cybersicherheitswarnungen/DE/2021/2021-549177-1032.html

.. [5]
   BSI Pressemitteilung zu Log4Shell:
   https://www.bsi.bund.de/DE/ServiceNavi/Presse/Pressemitteilungen/Presse2021/211211_log4Shell_WarnstufeRot.html

.. [6]
   BayLDA Meldung einer Datenschutzverletzung nach Art. 33 DS-GVO für bayerische
   Verantwortliche aus dem nicht-öffentlichen Bereich:
   https://www.lda.bayern.de/datenschutzverletzung

.. [7]
   BayLDA Cybersicherheit Checkliste mit Prüfkriterien nach Art. 32 DS-GVO:
   https://www.lda.bayern.de/media/checkliste/baylda_checkliste_medizin.pdf (ohne Kapitel 9)

.. [8]
   BayLDA Patch Management Checkliste nach Art. 32 DS-GVO:
   https://www.lda.bayern.de/media/checkliste/baylda_checkliste_patch_mgmt.pdf

.. [9]
   Cyberabwehr Bayern - Ansprechpartner zur Cybersicherheit in Bayern:
   https://www.lda.bayern.de/media/Behoerdenuebersicht_Cybersicherheitsvorfall.pdf

.. [10]
   Zentrale Ansprechstelle Cybercrime (ZAC) des Bayerischen Landeskriminalamtes (LKA):
   https://www.polizei.bayern.de/kriminalitaet/internetkriminalitaet/002464/index.html

.. [11]
   https://datenschutz.hessen.de/pressemitteilungen/unmittelbarer-handlungsbedarf-wegen-schwachstelle-in-java-bibliothek-log4j

.. [12]
   BSI Kritische Schwachstelle in Java-Bibliothek Log4j
   https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Webanwendungen/log4j/log4j_node.html

.. [13]
   Aktuelle Fassung der Cyber-Sicherheitswarnung
   Neu: Version 1.0: Kritische "Log4Shell" Schwachstelle in weit verbreiteter Protokollierungsbibliothek Log4j (14.12.2021)
   https://www.bsi.bund.de/SharedDocs/Cybersicherheitswarnungen/DE/2021/2021-549177-1032.pdf?__blob=publicationFile&v=3

.. [14]
   Detektion und Reaktion
   Arbeitspapier Detektion und Reaktion Log4j Schwachstelle, Version 1.3
   https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/Vorfaelle/log4j-Schwachstelle-2021/log4j_Schwachstelle_Detektion_Reaktion.pdf?__blob=publicationFile&v=5

.. [15]
   Meldungen des BSI
   Version 1.5: Kritische Schwachstelle in log4j veröffentlicht (17.12.2021)
   https://www.bsi.bund.de/SharedDocs/Cybersicherheitswarnungen/DE/2021/2021-549032-10F2.pdf?__blob=publicationFile&v=10

.. [16]
   Pressemitteilung vom 11. Dezember 2021
   https://www.bsi.bund.de/DE/Service-Navi/Presse/Pressemitteilungen/Presse2021/211211_log4Shell_WarnstufeRot.html

.. [17]
   Pressekonferenz am 13.12.2021, 15 Uhr (Stream der Tagesschau)
   https://www.tagesschau.de/multimedia/video/video-960365.html

.. [18]
   Empfehlungen für Verbraucherinnen und Verbraucher
   Sicherheitslücke "Log4Shell" gefährdet Systeme weltweit
   https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Cyber-Sicherheitslage/Schwachstelle-log4Shell-Java-Bibliothek/log4j_node.html

.. [19]
   CVE-2021-45105
   https://nvd.nist.gov/vuln/detail/CVE-2021-45105

.. [20]
   Detektion und Reaktion
   Arbeitspapier Detektion und Reaktion Log4j Schwachstelle, Version 1.4
   https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/Vorfaelle/log4j-Schwachstelle-2021/log4j_Schwachstelle_Detektion_Reaktion.pdf;jsessionid=54466E46B18714A934C513FA464CD1AF.internet081?__blob=publicationFile&v=6

.. [21]
   G. Linares, 2021. [Online].
   https://twitter.com/Laughing_Mantis/status/1470412026119798786. 

.. [22]
   Elastico, 2021. [Online].
   https://discuss.elastic.co/t/apache-log4j2-remote-code-executionrce-vulnerability-cve-2021-44228-esa-2021-31/291476. 
