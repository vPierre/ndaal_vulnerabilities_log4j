******************************************************************************
ndaal - Vulnerabilities - log4j - Introduction
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Introduction:

ndaal - Vulnerabilities - log4j - Introduction
------------------------------------------------------------------------------

Here is a little introduction and the recommendation for a fix.

CVE-2021-44228
##############################################################################

Apache Log4j2 <=2.14.1 JNDI [1]_ features used in configuration, log messages, 
and parameters do not protect against attacker controlled LDAP and other JNDI 
related endpoints. An attacker who can control log messages or log message 
parameters can execute arbitrary code loaded from LDAP servers when message 
lookup substitution is enabled. From log4j 2.15.0, this behavior has been 
disabled by default. In previous releases (>2.10) this behavior can be 
mitigated by setting system property "log4j2.formatMsgNoLookups" to “true” or 
it can be mitigated in prior releases (<2.10) by removing the JndiLookup class 
from the classpath 
(example: ``zip -q -d log4j-core-*.jar org/apache/logging/log4j/core/lookup/JndiLookup.class`` ).

.. Warning::
   CVSS score: 10.0

.. figure:: 
   _static/JNDI.png
   :alt: JNDI
   :width: 75%
   :align: center

CVE-2021-45046
##############################################################################

It was found that the fix to address CVE-2021-44228 [1]_ in Apache Log4j 
2.15.0 was incomplete in certain non-default configurations. This could allows 
attackers with control over Thread Context Map (MDC) input data when the 
logging configuration uses a non-default Pattern Layout with either a Context 
Lookup (for example, $${ctx:loginId}) or a Thread Context Map pattern 
(%X, %mdc, or %MDC) to craft malicious input data using a JNDI Lookup pattern 
resulting in a denial of service (DOS) attack. Log4j 2.15.0 restricts JNDI 
LDAP lookups to localhost by default. Note that previous mitigations 
involving configuration such as to set the system property 
`log4j2.noFormatMsgLookup` to `true` do NOT mitigate this specific 
vulnerability. Log4j 2.16.0 fixes this issue by removing support for message 
lookup patterns and disabling JNDI functionality by default. This issue can 
be mitigated in prior releases (<2.16.0) by removing the JndiLookup class 
from the classpath 

(example: ``zip -q -d log4j-core-*.jar`` ``org/apache/logging/log4j/core/lookup/JndiLookup.class``).

.. Warning::
   CVSS score: 9.0

CVE-2021-45105
##############################################################################

Apache Log4j2 versions 2.0-alpha1 through 2.16.0 (excluding 2.12.3) did not 
protect from uncontrolled recursion from self-referential lookups. This 
allows an attacker with control over Thread Context Map data to cause a denial 
of service when a crafted string is interpreted. This issue was fixed in 
Log4j 2.17.0 [3]_ and 2.12.3.

Mitigation
Log4j 1.x mitigation
Log4j 1.x is not impacted by this vulnerability.

Log4j 2.x mitigation
Implement one of the following mitigation techniques:

Java 8 (or later) users should upgrade to release 2.17.1 or newer.

.. Warning::
   CVSS score: 7.5

CVE-2021-44832
##############################################################################

Apache Log4j2 versions 2.0-beta7 through 2.17.0 (excluding security fix 
releases 2.3.2 and 2.12.4) are vulnerable to a remote code execution (RCE) 
attack where an attacker with permission to modify the logging configuration 
file can construct a malicious configuration using a JDBC Appender with a 
data source referencing a JNDI URI which can execute remote code. This issue 
is fixed by limiting JNDI data source names to the java protocol in Log4j2 
versions 2.17.1, 2.12.4, and 2.3.2.

.. Warning::
   CVSS score: 6.6

CVE-2021-4104
##############################################################################

JMSAppender in Log4j 1.2 is vulnerable to deserialization of untrusted data 
when the attacker has write access to the Log4j configuration. The attacker 
can provide ``TopicBindingName`` and ``TopicConnectionFactoryBindingName`` 
configurations causing ``JMSAppender`` to perform JNDI requests that result in 
remote code execution in a similar fashion to CVE-2021-44228. Note this issue
only affects Log4j 1.2 when specifically configured to use JMSAppender, which 
is not the default. Apache Log4j 1.2 reached end of life in August 2015. 
Users should upgrade to Log4j 2 as it addresses numerous other issues from the
previous versions.

.. Warning::
   CVSS score: 8.1

Patch
##############################################################################

.. Note::
   Patch at least to >=2.17.1 [2]_ , [3]_ , [4]_ and [5]_ 

Documentation
##############################################################################

The Log4j 2 User`s Guide is available on this site [7]_ or as a downloadable 
PDF [8]_ .

Other - similar attack pattern
##############################################################################

Like Log4Shell, the bug relates to JNDI (Java Naming and Directory Interface) 
"remote class loading." JNDI is an API that provides naming and directory 
functionality for Java apps. It means that if an attacker can get a malicious 
URL into a JNDI lookup, it could enable RCE.

"In a nutshell, the root cause is similar to Log4Shell - several code paths in
the H2 database framework pass unfiltered attacker-controlled URLs to the 

``javax.naming.Context.lookup`` function, which allows for remote codebase 
loading (AKA Java code injection AKA remote code execution)," 
JFrog explained. [9]_ and [10]_ 

"Specifically, the ``org.h2.util.JdbcUtils.getConnection`` method takes a 
driver class name and database URL as parameters. If the driver`s class is 
assignable to the javax.naming.Context class, the method instantiates an 
object from it and calls its lookup method."

.....

.. Seealso::
   :ref:`Vulnerabilities - log4j - Advisories list here <Section_Vulnerabilities_-_log4j_-_Advisories>`

.....

.. Rubric:: Footnotes

.. [1]
   CVE-2021-44228
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228

.. [2]
   CVE-2021-45046
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046

.. [3]
   CVE-2021-45105
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105

.. [4]
   Apache Log4j Security Vulnerabilities
   https://logging.apache.org/log4j/2.x/security.html

.. [5]
   CVE-2021-44832
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44832

.. [6]
   CVE-2021-4104
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-4104

.. [7]
   Log4j 2 User`s Guide html
   https://logging.apache.org/log4j/2.x/manual/index.html

.. [8]
   Log4j 2 User`s Guide PDF
   https://logging.apache.org/log4j/2.x/log4j-users-guide.pdf

.. [9]
   Researchers Warn of New Log4Shell-Like Java Vulnerability
   https://www.infosecurity-magazine.com/news/researchers-new-log4shelllike-java/

.. [10]
   CVE-2021-42392
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-42392
