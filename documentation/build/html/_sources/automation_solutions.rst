******************************************************************************
Automation Solution with Debian Linux for Linux and Windows
******************************************************************************

.. sectionauthor:: Alaa Jubakhanji <Alaa.Jubakhanji@ndaal.eu>

.. contents:: Content
   :depth: 3

.. _Automation_Solution_with_Debian_Linux_for_Linux_and_Windows:

log4shell-detector
------------------------------------------------------------------------------

This chapter is a walk-through of how a combination of an open source
**log4shell** Python-based detector and a set of Python automation tools were 
used to automatically detect exploitations on a system. Moreover, a second tool,
the **log4j-sniffer** is later explored under the 
:ref:`section **log4j-sniffer** <Chapter_Automation_Solution_log4j-sniffer>`.

The log4shell-detector, created by Florian Roth on December 2021 following the 
**log4j** CVE-2021-44228 [1]_ 
(and further weaknesses represented by [2]_ and [3]_ )
vulnerability exposure, is an open source detection tool maintained 
by at least 12 **contributors** including **ndaal**.

The detector checks local log files for indicators of exploitation attempts, 
even heavily obfuscated ones that string or regular expression based patterns 
wouldn't detect.
This is the sole purpose of this tool. The detector neither finds vulnerable 
applications nor verifies if an exploitation attempt was successful. [1]_

What makes the **log4j** CVE-2021-44228 exploitation problematic is how 
heavily strings can be obfuscated in many different ways. The idea behind the 
detector is that the respective characters have to appear in a log line in a 
certain order to match.::

    ${jndi:ldap:

List-like the string is as follows:::

    ['$', '{', 'j', 'n', 'd', 'i', ':', 'l', 'd', 'a', 'p', ':']


The detector processes each respective line in a log file character by 
character. If the character matches a character in the previous list, a 
pointer moves forward.

When the pointer reached the end of the list, the triggered detection returns 
the following in a log file, where logged lined consists of the following:

#. File name,

#. The line number,

#. Complete log line,

#. The detected string. [1]_

Running the log4shell-detector
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You may execute the detection with:::

    python3 log4shell-detector.py -p /var/log  [4]_ and [7]_ 


If python3 isn't available, you may also execute using python. [4]_

|

You can also try to find evidence of the log4j package usage by running the following commands:

    ``ps aux | egrep '[l]og4j'`` [5]_ 

    ``find / -iname "log4j*"``

    ``lsof | grep log4j`` [6]_ 

    ``find . -name '*[wj]ar' -print -exec sh -c 'jar tvf {} | grep log4j' \;``

    ``grep -r --include *.[wj]ar "JndiLookup.class" / 2>&1 | grep matches`` 

    ``locate log4j | grep -v log4js``

   If you want to investigate exploitation attempts in log try these commands.
   This command searches for exploitation attempts in uncompressed files in 
   folder ``/var/log`` and all sub folders.

   ``sudo egrep -i -r '∖$∖{jndi:(ldap[s]?|rmi|dns):/[^∖n]+' /var/log``

   This command searches for exploitation attempts in compressed files in 
   folder ``/var/log`` and all sub folders

   ``sudo find /var/log -name ∖*.gz -print0 | xargs -0 zgrep -E -i '∖$∖{jndi:(ldap[s]?|rmi|dns):/[^∖n]+'``

   To find occurences of breach in a Windows based system, run this command in
   PowerShell

   ``gci 'C:∖' -rec -force -include *.jar -ea 0 | foreach {select-string "JndiLookup.class" $_} | select -exp Path``

If none of these commands returned a result, you should be safe. [4]_

or 

.. Rubric:: with grep

``grep -P '(?im)(?:^|[\n]).*?(?:[\x24]|%(?:25%?)*24|\\u?0*(?:44|24))(?:[\x7b]|%(?:25%?)*7b|\\u?0*(?:7b|173))[^\n]*?((?:j|%(?:25%?)*(?:4a|6a)|\\u?0*(?:112|6a|4a|152))[^\n]*?(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))[^\n]*?(?:[\x3a]|%(?:25%?)*3a|\\u?0*(?:72|3a))[^\n]*?((?:l|%(?:25%?)*(?:4c|6c)|\\u?0*(?:154|114|6c|4c))[^\n]*?(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:a|%(?:25%?)*(?:41|61)|\\u?0*(?:101|61|41|141))[^\n]*?(?:p|%(?:25%?)*(?:50|70)|\\u?0*(?:70|50|160|120))(?:[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163)))?|(?:r|%(?:25%?)*(?:52|72)|\\u?0*(?:122|72|52|162))[^\n]*?(?:m|%(?:25%?)*(?:4d|6d)|\\u?0*(?:4d|155|115|6d))[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))|(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))|(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))|(?:[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))){2}[^\n]*?(?:o|%(?:25%?)*(?:4f|6f)|\\u?0*(?:6f|4f|157|117))[^\n]*?(?:p|%(?:25%?)*(?:50|70)|\\u?0*(?:70|50|160|120))|(?:c|%(?:25%?)*(?:43|63)|\\u?0*(?:143|103|63|43))[^\n]*?(?:o|%(?:25%?)*(?:4f|6f)|\\u?0*(?:6f|4f|157|117))[^\n]*?(?:r|%(?:25%?)*(?:52|72)|\\u?0*(?:122|72|52|162))[^\n]*?(?:b|%(?:25%?)*(?:42|62)|\\u?0*(?:102|62|42|142))[^\n]*?(?:a|%(?:25%?)*(?:41|61)|\\u?0*(?:101|61|41|141))|(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))|(?:h|%(?:25%?)*(?:48|68)|\\u?0*(?:110|68|48|150))(?:[^\n]*?(?:t|%(?:25%?)*(?:54|74)|\\u?0*(?:124|74|54|164))){2}[^\n]*?(?:p|%(?:25%?)*(?:50|70)|\\u?0*(?:70|50|160|120))(?:[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163)))?)[^\n]*?(?:[\x3a]|%(?:25%?)*3a|\\u?0*(?:72|3a))|(?:b|%(?:25%?)*(?:42|62)|\\u?0*(?:102|62|42|142))[^\n]*?(?:a|%(?:25%?)*(?:41|61)|\\u?0*(?:101|61|41|141))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))[^\n]*?(?:e|%(?:25%?)*(?:45|65)|\\u?0*(?:45|145|105|65))[^\n]*?(?:[\x3a]|%(?:25%?)*3a|\\u?0*(?:72|3a))(JH[s-v]|[\x2b\x2f-9A-Za-z][CSiy]R7|[\x2b\x2f-9A-Za-z]{2}[048AEIMQUYcgkosw]ke[\x2b\x2f-9w-z]))' <logfile>``

.. Rubric:: Combine it with find to recursively scan a (sub-)folder of log files

``find /var/log -name "*.log" | xargs grep -P '(?im)(?:^|[\n]).*?(?:[\x24]|%(?:25%?)*24|\\u?0*(?:44|24))(?:[\x7b]|%(?:25%?)*7b|\\u?0*(?:7b|173))[^\n]*?((?:j|%(?:25%?)*(?:4a|6a)|\\u?0*(?:112|6a|4a|152))[^\n]*?(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))[^\n]*?(?:[\x3a]|%(?:25%?)*3a|\\u?0*(?:72|3a))[^\n]*?((?:l|%(?:25%?)*(?:4c|6c)|\\u?0*(?:154|114|6c|4c))[^\n]*?(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:a|%(?:25%?)*(?:41|61)|\\u?0*(?:101|61|41|141))[^\n]*?(?:p|%(?:25%?)*(?:50|70)|\\u?0*(?:70|50|160|120))(?:[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163)))?|(?:r|%(?:25%?)*(?:52|72)|\\u?0*(?:122|72|52|162))[^\n]*?(?:m|%(?:25%?)*(?:4d|6d)|\\u?0*(?:4d|155|115|6d))[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))|(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))|(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))|(?:[^\n]*?(?:[i\x{130}\x{131}]|%(?:25%?)*(?:49|69|C4%(?:25%?)*B0|C4%(?:25%?)*B1)|\\u?0*(?:111|69|49|151|130|460|131|461))){2}[^\n]*?(?:o|%(?:25%?)*(?:4f|6f)|\\u?0*(?:6f|4f|157|117))[^\n]*?(?:p|%(?:25%?)*(?:50|70)|\\u?0*(?:70|50|160|120))|(?:c|%(?:25%?)*(?:43|63)|\\u?0*(?:143|103|63|43))[^\n]*?(?:o|%(?:25%?)*(?:4f|6f)|\\u?0*(?:6f|4f|157|117))[^\n]*?(?:r|%(?:25%?)*(?:52|72)|\\u?0*(?:122|72|52|162))[^\n]*?(?:b|%(?:25%?)*(?:42|62)|\\u?0*(?:102|62|42|142))[^\n]*?(?:a|%(?:25%?)*(?:41|61)|\\u?0*(?:101|61|41|141))|(?:n|%(?:25%?)*(?:4e|6e)|\\u?0*(?:4e|156|116|6e))[^\n]*?(?:d|%(?:25%?)*(?:44|64)|\\u?0*(?:44|144|104|64))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))|(?:h|%(?:25%?)*(?:48|68)|\\u?0*(?:110|68|48|150))(?:[^\n]*?(?:t|%(?:25%?)*(?:54|74)|\\u?0*(?:124|74|54|164))){2}[^\n]*?(?:p|%(?:25%?)*(?:50|70)|\\u?0*(?:70|50|160|120))(?:[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163)))?)[^\n]*?(?:[\x3a]|%(?:25%?)*3a|\\u?0*(?:72|3a))|(?:b|%(?:25%?)*(?:42|62)|\\u?0*(?:102|62|42|142))[^\n]*?(?:a|%(?:25%?)*(?:41|61)|\\u?0*(?:101|61|41|141))[^\n]*?(?:[s\x{17f}]|%(?:25%?)*(?:53|73|C5%(?:25%?)*BF)|\\u?0*(?:17f|123|577|73|53|163))[^\n]*?(?:e|%(?:25%?)*(?:45|65)|\\u?0*(?:45|145|105|65))[^\n]*?(?:[\x3a]|%(?:25%?)*3a|\\u?0*(?:72|3a))(JH[s-v]|[\x2b\x2f-9A-Za-z][CSiy]R7|[\x2b\x2f-9A-Za-z]{2}[048AEIMQUYcgkosw]ke[\x2b\x2f-9w-z]))'``

Log4shell-detector semantic logic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


The detector's log file semantic logic consists of the following three 
categories referring to the file state:

.. csv-table:: Log4shell detector's loging logic
   :class: longtable
   :file: _IPs/log_logic.csv
   :widths: 10, 20
   :header-rows: 2

Extracting log file information with automated tools
------------------------------------------------------------------------------

Our log4shell research team at ndaal automated two tools to ease the process 
of information extraction from the detector's output log files.

The purpose of this is to avoid any manual investigations which comes in 
handy for large log file and/or running the detector on multiple systems 
multiple times.

Tool 1: Extracting exploited and unprocessed files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The first automated tool has two main functionalities. It begins by extracting
all relevant information regarding the files which flagged an exploitation and 
then iterates through all flagged files to highlight the unique files to avoid 
repetitive file checking.

The first step returns a file of the following syntax visualized with the 
following table:

.. csv-table:: Tool 1: First Extraction (Unique Exploited Files)
   :class: longtable
   :file: _IPs/tool1_1.csv
   :widths: 30
   :header-rows: 1

Secondly, the tool detects all files which went unprocessed by the detector. 
This is helpful for quickly finding possibly exploited unprocessed files.

The second step returns a file of the following syntax visualized with the 
following table:

.. csv-table:: Tool 1: Second Extraction (Unprocessed Files)
   :class: longtable
   :file: _IPs/tool1_2.csv
   :widths: 30
   :header-rows: 1

Both extractions are performed by following and understanding the detector's 
output semantic logic.

Tool 2: Extract and detect black/grey listed IP addresses
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The second automated tool also has two main functionalities it initially 
iterates through the output log file and extracts all IP addresses from it.
Next, the tool matches these IP addresses with the known list of 
black-listed IP addresses from our an ndaal gitlab repository which is 
constantly being updated with new known black-listed IP's.

..

Finally, the tool divides the extracted IP's into two lists:

#. Black IP's which includes all addresses which matched to the list.

#. Grey IP's which includes all addresses which were not matched.

The two outputs are visualized in the tables below:

.. csv-table:: Tool 2: Black IP's
   :class: longtable
   :file: _IPs/black.csv
   :widths: 30
   :header-rows: 1

.. csv-table:: Tool 2: Grey IP'S
   :class: longtable
   :file: _IPs/grey.csv
   :widths: 30
   :header-rows: 1

What are the Limitations of Binary Based Identification?
------------------------------------------------------------------------------

The flexibility of the JAR format allows for JAR files to take many shapes 
and forms.
They can have several levels of hierarchy (a JAR, within a JAR, within a JAR, 
etc.) and have different extensions. Each type usually reflects a different 
packaging and deployment schema and can have specific requirements.

Common examples include:

-  **TAR**; 
   Tape Archive (uncompressed)
-  **WAR**; 
   Web Application Archive. Should contain a web.
   xml file in the WEB-INF folder
-  **EAR**;
   Enterprise Application Archive
-  **SAR**;
   Service Application Archive
-  **PAR**;
   Portlet Archive
-  **RAR**;
   Resource Adapter. A system-level driver that connects a Java application to
   an enterprise information system. Contains source code and an ra.xml that 
   serves as a deployment descriptor
-  **KAR**; 
   Apache Karaf Archive. A special type of artifact that package a 
   features XML and all resources described in the features of this XML 
   (bundle JARs)
-  **Uber JAR/Fat JAR**;
   a JAR that contains a Java program as well as all of its dependencies. 
   The JAR serves as an all-in-one distribution of the software, without 
   needing any other Java code. It can be shaded, unshaded, or nested

   -  **Unshaded JAR**;
      Unpack all JAR files, then repack them into a single JAR
   -  **Shaded JAR**; 
      same as unshaded, but rename (i.e., "shade") all packages of all 
      dependencies
   -  **Nested JAR**;
      a JAR file that contains all other required JAR files (can have several 
      levels of hierarchy)

Other
------------------------------------------------------------------------------

.. Seealso::
   We created a pull request on Github for log4shell-detector by Neo23x0
   here [7]_ 

.. _Chapter_Automation_Solution_log4j-sniffer:

log4j-sniffer
---------------

Following the burst of the log4j vulnerability late in 2021, many open source tools were developed to help identify vulnerable systems.
We previously discussed one of those tools, the log4shell-detector developed by Florian Roth. Now, we explore another tool known as log4j-sniffer.
Within a specified directory, the log4j-sniffer crawls for all instances of log4j which are earlier than version 2.16. The open source tool is designed to detect vulnerable instances of log4j within a directory tree.

Scanning for versions affected by CVE-2021-44228, CVE-2021-45046, CVE-2021-45105 and CVE-2021-44832 is currently also supported. [8]_

The detector scans a filesystem looking for all types of the following types based upon the suffix:

- Zips: .zip
- Java archives: .jar, .war, .ear
- Tar: .tar, .tar.gz, .tgz, .tar.bz2, .tbz2

Conveniently enough, this tool ensures that archives containing other archives are also recursively inspected up to a configurable maximum depth.
For output options on nested archive inspection, run::

    log4j-sniffer crawl --help


More specifically, the sniffer will look for the following:

- Jar files matching **log4j-core-<version>.jar**, including those nested within another archive,

- Class files named org.apache.logging.log4j.core.net.JndiManager within Jar files or other archives and check against md5 hashes of known versions,

- Class files named JndiManager in other package hierarchies and check against md5 hashes of known versions,

- Matching of the bytecode of classes named JndiManager against known classes (see below for more details),

- Matching of bytecode within obfuscated or shaded jars for partial matches against known classes (see below).

Installing the log4j-sniffer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If Go is available on the host system, the following command can be used to install this program:::

    go install github.com/palantir/log4j-sniffer@latest

The following repository also publishes binaries that can be downloaded and executed.::

    https://github.com/palantir/log4j-sniffer


log4j-sniffer executables compiled for linux-amd64, darwin-amd64, darwin-arm64 and windows-amd64 architectures are available on:::

    https://github.com/palantir/log4j-sniffer/releases

log4j-sniffer on Linux
^^^^^^^^^^^^^^^^^^^^^^^^^^^

This tool is intensive and is recommended to be run with low priority settings.

On Linux:::

    ionice -c 3 nice -n 19 log4j-sniffer crawl /path/to/a/directory

Output for vulnerable files looks as follows:::

    [INFO] Found archive with name matching vulnerable log4j-core format at examples/single_bad_version/log4j-core-2.14.1.jar
    [INFO] Found JndiManager class that was an exact md5 match for a known version at org/apache/logging/log4j/core/net/JndiManager.class
    [INFO] Found JndiLookup class in the log4j package at org/apache/logging/log4j/core/lookup/JndiLookup.class
    [MATCH] CVE-2021-44228, CVE-2021-45046, CVE-2021-45105, CVE-2021-44832 detected in file examples/single_bad_version/log4j-core-2.14.1.jar. log4j versions: 2.14.0-2.14.1, 2.14.1. Reasons: JndiLookup class and package name matched, jar name matched, JndiManager class and package name matched, class file MD5 matched
    Files affected by CVE-2021-44228 or CVE-2021-45046 or CVE-2021-45105 or CVE-2021-44832 detected: 1 file(s)
    1 total files scanned, skipped identifying 0 files due to config, skipped 0 paths due to permission denied errors, encountered 0 errors processing paths

log4j-sniffer on mac
^^^^^^^^^^^^^^^^^^^^

Download the latest version

- Locate releases.

- You will need a different asset depending on the generation of your Mac.

    - Select the asset with “macos-amd” in the file name for older Intel Macs.

    - Select “macos-arm” for newer m1 Macs

- Confirm that the file is downloading to your “Downloads” folder.

- Once the download is complete, click on the file to open.

- Drag and drop the “log4j-sniffer” icon into your Downloads through the Finder.

    - Open a Finder window by searching for “Finder” using the magnifying glass on the top right of your screen, or selecting the icon in your Dock.

    - Drag and drop the “log4j-sniffer” icon into Downloads.

- Open the Terminal by searching for “Terminal” using the magnifying glass in the top right corner of the screen.

To crawl the entire system run:::

    ~/Downloads/log4j-sniffer crawl / --ignore-dir="^/dev"


To crawl specific folders:::

    ~/Downloads/log4j-sniffer crawl /PATH/TO/YOUR/FOLDER

If your computer is unable to locate log4j-sniffer, you may have to make it executable before using it. In your terminal, run the following:::

    chmod +x ~/Downloads/log4j-sniffer
    ./log4j-sniffer crawl /PATH/TO/YOUR/FOLDER


log4j-sniffer on Windows
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Download the latest version

- Locate [releases] (https://github.com/palantir/log4j-sniffer/releases).

- Select the Windows asset.

- Confirm that the file is downloading to your “Downloads” folder.

- Type "Command Prompt" into the search bar at the bottom and in the right pane click "Run as administrator".

- Navigate to your Downloads folder, e.g. cd C:\Users\yourname\Downloads

- Run log4j-sniffer-1.1.0-windows-amd64.exe crawl C:\ to crawl the entire system, substituting the drive of your choice, e.g. C:\, D:\

    - Run log4j-sniffer-1.1.0-windows-amd64.exe crawl C:\PATH\TO\YOUR\FOLDER to crawl specific folders

log4j-sniffer primary usage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Log4j-sniffer's primary command for the tool is **crawl** which taken an argument that is the path to the directory to be crawled. Thus, the standard usage is:::

    log4j-sniffer crawl [pathToDirectory]


The standard mode prints output in a human-readable format and prints a summary that states the number of vulnerabilities found after running.

Specifying the --json flag makes it such that the output of the program is all in JSON: each line of output is JSON that describes the vulnerability that is found, and if summary mode is enabled then the final summary is also output as a line of JSON. [8]_

Here is an example of the output with --json:::


    {"message":"CVE-2021-44228, CVE-2021-44832, CVE-2021-45046, CVE-2021-45105 detected","filePath":"examples/inside_a_dist/wrapped_log4j.tar.gz","detailedPath":"examples/inside_a_dist/wrapped_log4j.tar.gz!log4j-core-2.14.1.jar","cvesDetected":["CVE-2021-44228","CVE-2021-44832","CVE-2021-45046","CVE-2021-45105"],"findings":["jndiLookupClassPackageAndName","jarNameInsideArchive","jndiManagerClassPackageAndName","classFileMd5"],"log4jVersions":["2.14.0-2.14.1","2.14.1"]}
    {"filesScanned":1,"permissionDeniedErrors":0,"pathErrors":0,"pathsSkipped":0,"numImpactedFiles":1}


log4j-sniffer semantic logic
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The JSON fields have the following meaning:

.. csv-table:: Log4j-sniffer semantic logic
   :class: longtable
   :file: _IPs/log2_logic.csv
   :widths: 30, 30
   :header-rows: 1

The findings array reports the following possible values:

.. csv-table:: Log4j-sniffer semantic logic
   :class: longtable
   :file: _IPs/findings.csv
   :widths: 30, 30
   :header-rows: 1

The output summary outlines the following:

- filesScanned: the total number of files crawled

- permissionDeniedErrors: the number of directories or files that could not be read due to permissions

- pathErrors: the number of paths where an unexpected error occurred while trying to identify bad log4j versions

- pathsSkipped: the numbers of paths skipped from full identification of bad log4j versions due to the config options set

- numImpactedFiles: the total number of files impacted

- findings: the total number of findings previously output. For file path only mode, this will equal the number of impacted files.

Specifying --summary=false makes it such that the program does not output a summary line at the end. In this case, the program will only print output if vulnerabilities are found. [8]_


.....

.. Rubric:: Footnotes

.. [1]
   CVE-2021-44228
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228

.. [2]
   CVE-2021-45046
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45046

.. [3]
   CVE-2021-45105
   https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-45105

.. [4]
   https://github.com/Neo23x0/log4shell-detector

.. [5]
   ps displays information about a selection of the active processes. If 
   you want a repetitive update of the selection and the displayed 
   information, use top(1) instead.

.. [6]
   lsof: list open files.
   An open file may be a regular file, a directory, a block special file, a 
   character special file, an executing text reference, a library, a stream 
   or a network file (Internet socket, NFS file or UNIX domain socket.) A 
   specific file or all the files in a file system may be selected by path. 
   Instead of a formatted display, lsof will produce output that can be 
   parsed by other programs. In addition to producing a single output list, 
   lsof will run in repeat mode. In repeat mode it will produce output, 
   delay, then repeat the output operation until stopped with an interrupt 
   or quit signal.

.. [7]
   pull request on Github for log4shell-detector 
   https://github.com/Neo23x0/log4shell-detector/pull/59

.. [8]
   https://github.com/palantir/log4j-sniffer
