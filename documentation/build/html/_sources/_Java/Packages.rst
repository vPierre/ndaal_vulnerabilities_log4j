Java Packages
==============================================================================

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Java_Packages:

To help out, Google has pulled together a list of the 500 most-used and 
impacted **Java Code packages**. [1]_ 

.. csv-table:: 500 most-used and impacted Java code packages
   :file: log4j_top_500_dependents.csv
   :widths: 100,15,15
   :header-rows: 1

.....

.. Rubric:: Footnotes

.. [1]
   Google Security Blog
   Understanding the Impact of Apache Log4j Vulnerability, December 17, 2021
   https://security.googleblog.com/2021/12/understanding-impact-of-apache-log4j.html
