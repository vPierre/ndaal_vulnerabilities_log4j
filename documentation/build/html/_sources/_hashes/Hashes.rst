******************************************************************************
ndaal - Vulnerabilities - log4j - Hashes
******************************************************************************

.. sectionauthor:: Pierre Gronau <Pierre.Gronau@ndaal.eu>

..

.. contents:: Content
   :depth: 3

.. _Section_Hashes:

We use these hashes to detect vulnerable **LOG4J** versions 
(sources: [1]_ and [2]_ ).

Checkums by Apache
------------------------------------------------------------------------------

These hashes/checksums are created by Apache and store on their archive.

md5sum by Apache
##############################################################################

You SHOULD prevent using md5 checksums because of their weakness.

.. csv-table:: log4j hashes md5
   :file: md5sum.txt
   :widths: 80
   :header-rows: 1

sha1sum by Apache
##############################################################################

You SHOULD prevent using sha1 checksums because of their weakness.

.. csv-table:: log4j hashes sha1
   :file: sha1sum.txt
   :widths: 80
   :header-rows: 1

sha256sum by Apache
##############################################################################

.. csv-table:: log4j hashes sha256
   :file: sha256sum.txt
   :widths: 80
   :header-rows: 1

sha512sum by Apache
##############################################################################

.. csv-table:: log4j hashes sha512
   :file: sha512sum.txt
   :widths: 160
   :header-rows: 1


.. Note::
   For convenience you can download the hashes files here [3]_ 

other topic
##############################################################################

We open two issues because of the wrong representation of some hashes [4]_ 
and [5]_ in the archives of Apache log4j.

vulnerable log4j hashes by other sources
------------------------------------------------------------------------------

The repository contains a CSV file [6]_ with names of **log4j JAR** files 
along with their **SHA1 hashes** for each version that is known to be 
vulnerable. The originate source will be found here [7]_ 

Evaluating if a log4j JAR is vulnerable
------------------------------------------------------------------------------

- Calculate e.g. the ``SHA1`` sum for the file

   - Linux: ``sha1sum <path/to/file>``
   - Windows (in Powershell): ``get-filehash -Algorithm SHA1 <path\to\file>``
   - Mac: ``shasum </path/to/file>``

- Check if the hash generated in step 1 exists

- If the hash exists, the **log4j JAR** is a **vulnerable version**.

.....

.. Rubric:: Footnotes

.. [1]
   https://github.com/scstanton/log4j-hashes
   https://github.com/scstanton/log4j-hashes/issues/1
   https://github.com/scstanton/log4j-hashes/issues/2

.. [2]
   Apache archives
   https://archive.apache.org/dist/logging/log4j/

.. [3]
   https://gitlab.com/vPierre/ndaal_vulnerabilities_log4j/-/tree/main/documentation/source/_hashes

.. [4]
   https://issues.apache.org/jira/browse/LOG4J2-3324

.. [5]
   https://issues.apache.org/jira/browse/LOG4J2-3325

.. [6]
   https://gitlab.com/vPierre/ndaal_vulnerabilities_log4j/-/blob/main/documentation/source/_hashes/log4j-vuln-versions-sha1sum.csv

.. [7]
   https://github.com/Kloudle/vulnerable-log4j-jar-hashes
