.. image:: 
   documentation/source/_static/Logo.png
   :align: center

.. contents:: Content
    :depth: 3

log4j vulnerabilities
------------------------------------------------------------------------------

Hello and welcome to our repo.

Here you can find a summary research of the log4j vulnerability.

- how can i determine if my application is affected

- where can I find additional information from the application vendor?

- how can I protect myself against this vulnerability?

You can find the different output formats at:

- html: /documents/build/html/index.html

- pdf: /documents/build/latex/ndaal-vulnerabilities-log4j.pdf

- epub: /documents/build/epub/ndaal-Vulnerabilities-log4j.epub

... and many others.

The information in this repo is continuously updated.


   **AddOn**

   Link to our IP list that already use the vulnerability:

   https://gitlab.com/vPierre/ndaal_vulnerabilities_log4j/-/tree/main/documentation/source/_IPs


This repo is brought to you by: 

**ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co. KG**

*The experts in IT Security, IT Compliance, Cloud and Automation.*

shorts
------------------------------------------------------------------------------

- 🔭 we are currently working on - automation and security
- 👯 our (log4j research) Team working for you: 

   - Alaa Jubakhanji
   - André Breuer
   - Ayesha Shafqat
   - Carsten Dingendahl
   - Kyal Smith
   - Mamoona Aslam
   - Pierre Gronau
   - Tobias Feuling
   - Vidhya Sasidharan

- 💬 Ask us about - info@ndaal.eu
- 🥅 2023 Goal - make IT better and more secure

Contact us
------------------------------------------------------------------------------

- www https://www.ndaal.eu/
- Linkedin https://de.linkedin.com/company/ndaal
